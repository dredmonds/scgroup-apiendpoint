SC-API
====

This repo was tested with Node.js `v6.9.x`.

## Setup in your local environment

Clone this repo on your machine:
```
git clone git@github.com:TheAppGarden/SC-API.git
```

Then navigate to this repo (`cd SC-API`) then run the command:

```
npm install
```

### Setup MongoDB locally

You need to install MongoDB on your machine - Make sure that you also install the `mongorestore` command/utility.

Once setup is complete, you should can launch the MongoDB server using the command:

```
service mongod start
```

Or you can just run the `mongod` command if you want to see the logs.

If it throws an error, you may need to create an empty `/data/db` directory (use `mkdir -p /data/db`).

Ask an existing developer for a MongoDB snapshot then unpack it and then import it to your local MongoDB instance using the `mongorestore` command.
You can provide the entire directory (containing all the .json and .bson files) like this:

```
mongorestore ../dump/switchboard/
```

Then you need to run this MongoDB query using a Mongo client (you can use the shell client by running the `mongo` command):

```
db.config.update({"_id" : ObjectId("57502fcbb0ffb2c88aaabd99")}, {$set: {pin: "localhost"} });
```

## Running the code

Once setup has completed, use this command to run the API server:

```
npm start
```

## Running the tests

```
npm test
```
