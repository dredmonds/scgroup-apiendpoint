var cluster = require('cluster');

if (cluster.isMaster) {
    var cpuCount = require('os').cpus().length;

    for (var i = 0; i < cpuCount; i += 1) {
        cluster.fork();
    }

    console.log('Running server on ' + require('os').cpus().length + ' threads.');

    cluster.on('exit', function () {
        cluster.fork();
    });
} else {
require('./server.js');
}
