var mongo = require('mongodb');
var uuid = require('node-uuid');
var bcrypt = require('bcrypt');
var formidable = require('formidable');
var fs = require('fs');
var xlsx = require('node-xlsx');
var generatePassword = require('password-generator');
var nodemailer = require('nodemailer');

var MongoClient = require('mongodb').MongoClient;
var Server = require('mongodb').Server;
var ObjectID = require('mongodb').ObjectID;
var db;

module.exports = function(args){
    var route = args.express();

    route.post('/addcodes', function(req, res) {
        var codes = [];
        var i = 0;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            console.log(files.fileField.name.split('.').pop());
            var filename = files.fileField.path;
            var obj = xlsx.parse(filename);
            var data = obj[0].data;
            var query = {};
            data.splice(0, 10);

            var bulk = req.db.collection('vpp_codes').initializeUnorderedBulkOp();
            for (i in data) {
                var temp = {
                    code: data[i][0],
                    url: data[i][2],
                    used: false
                }
                bulk.insert(temp);
            }
            bulk.execute(function (err, result) {
                if (err) throw err;
                res.status(200).send(result);
            });
        });
    });
    route.post('/addusers', function(req, res) {
        var users = [];
        var i = 0;
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            console.log(files.fileField.name.split('.').pop());
            var filename = files.fileField.path;
            var obj = xlsx.parse(filename);
            var data = obj[0].data;
            var query = {};
            data.splice(0, 10);

            var bulk = req.db.collection('valid_users').initializeUnorderedBulkOp();
            for (i in data) {
                var temp = {
                    data1: data[i][0],
                    data2: data[i][1]
                }
                bulk.insert(temp);
            }
            bulk.execute(function (err, result) {
                if (err) throw err;
                res.status(200).send(result);
            });
        });
    });

    route.get('/validate/:data1/:data2', function(req, res) {
        req.db.collection('valid_users').find({data1: req.params.data1, data2: req.params.data2}).toArray(function(err, docs) {
            if (err) throw err;
            if (docs.length == 0) {
                res.status(400).send(false);
            } else {
                res.status(200).send(true);
            }
        });
    });
    args.app.use('/auth', route);

}
