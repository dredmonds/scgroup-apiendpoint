var mongo = require('mongodb');

var MongoClient = require('mongodb').MongoClient;
var Server = require('mongodb').Server;
var ObjectID = require('mongodb').ObjectID;
var db;

module.exports = function(args){
    var route = args.express();
    route.get('/', function(req, res) {
        var ios;
        if (req.config.vpp == true) {
            ios = '/download/redeem';
        } else {
            ios = req.config.download.ios;
        }
        req.db.collection('vpp_codes').find({used: false}).toArray(function(err, docs) {
            if (err) throw err;
            res.render('download', {vpp: req.config.vpp, name: req.config.name, android: req.config.download.android, ios: ios, icon: req.config.icon, keys: docs.length});
        });
    });
    route.get('/redeem', function(req, res) {
        req.db.collection('vpp_codes').find({used: false}).toArray(function(err, docs) {
            if (docs.length == 0) return res.render('invalid');
            if (err) throw err;
            var code = docs[0];
            req.db.collection('vpp_codes').update({_id: ObjectID(code._id)},{ $set:{ used: true } }, function(err, result) {
                if (err) throw err;
                res.writeHead(302, {
                    'Location': code.url
                })
                res.end();
            });
        });
    });
    args.app.use('/download', route);

}
