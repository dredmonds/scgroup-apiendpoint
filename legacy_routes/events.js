var mongo = require('mongodb');
var s3 = require('../lib/s3');

var MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ObjectID = require('mongodb').ObjectID,
    db;

/*
Get all posts for 'Events' section
*/
exports.events = function(req, res) {

	//Open 'Events' collection
    req.db.collection('events', function(err, collection) {

	    //Look for all items and push them to array
        collection.find().toArray(function(err, items) {

	        //return JSON object
            res.jsonp(items);

        });

    });

};

/*
Create new item for 'Events' section
*/
exports.eventsNewItem = function(req, res) {

    var title = req.body.title;
	var location = req.body.location;
	var startTime = req.body.startTime;
	var endTime = req.body.endTime;
	var allDay = req.body.allDay;
	var additionalNotes = req.body.additionalNotes;
	var attendees = req.body.attendees;

    var itemData = [
	{
	   "title":title,
	   "location":location,
	   "startTime":startTime,
	   "endTime":endTime,
	   "allDay":allDay,
	   "additionalNotes":additionalNotes,
	   "attendees":attendees,
     "image": req.body.image
	}
    ];

    req.db.collection('events', function(err, collection) {
        collection.insert(itemData, {safe:true}, function(err, result) {
	      if (err){
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.write("fail...");
		console.log(err);
		res.end();
	      }else{
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.write("ok...");
		res.end();
	      }
        });
    });

};

/*
This function updates existing Question & Answer
*/
exports.eventsUpdateItem = function(req, res) {

    var id = req.body._id;

    var o_id = new ObjectID(id);

    var title = req.body.title;
	var location = req.body.location;
	var startTime = req.body.startTime;
	var endTime = req.body.endTime;
	var allDay = req.body.allDay;
	var additionalNotes = req.body.additionalNotes;
	var attendees = req.body.attendees;

    var itemData = {
	   "title":title,
	   "location":location,
	   "startTime":startTime,
	   "endTime":endTime,
	   "allDay":allDay,
	   "additionalNotes":additionalNotes,
	   "attendees":attendees,
     "image": req.body.image
	};

	req.db.collection('events').findAndModify(
	  {_id: o_id}, // query
	  [['_id','asc']],  // sort order
	  itemData, // replacement, replaces only the field "hi"
	  {}, // options
	  function(err, object) {
	      if (err){
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.write("fail...");
		console.log(err);
		res.end();
	      }else{
		res.writeHead(200, {"Content-Type":"text/plain"});
		res.write("ok...");
		res.end();
	      }
	});

};

/*
This function deletes existing Question & Answer
*/
exports.eventsDeleteItem = function(req, res) {

    var id = req.body._id;

    var o_id = new ObjectID(id);

    req.db.collection('events').find({_id: o_id}).toArray(function(err, result) {
      if (err) return res.status(400).send(err);
      var event = result[0];
      if (event.image) {
        s3.delete(event.image[event.image.length-2], event.image[event.image.length-1])
      }

      req.db.collection('events').findAndRemove(
        {'_id': o_id}, // query
        [['_id','asc']],  // sort order
        {}, // options
        function(err, object) {
            if (err){
      	res.writeHead(200, {"Content-Type":"text/plain"});
      	res.write("fail...");
      	console.log(err);
      	res.end();
            }else{
      	res.writeHead(200, {"Content-Type":"text/plain"});
      	res.write("ok...");
      	res.end();
            }
        });

    })

};
