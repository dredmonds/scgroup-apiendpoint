var mongo = require('mongodb');

var MongoClient = require('mongodb').MongoClient,
    Server = require('mongodb').Server,
    ObjectID = require('mongodb').ObjectID,
    db;

/*
Get all content for 'Library' section
*/
exports.library = function(req, res) {
	
	//Open 'Library' collection
    req.db.collection('library', function(err, collection) {
	    
	    //Look for all items and push them to array
        collection.find().toArray(function(err, items) {
	        
	        //return JSON object
            res.jsonp(items);
            
        });
        
    });
    
};

exports.libraryNewItem = function(req, res) {
	req.db.collection('library', function(err, collection) {
		var itemData = [{
			"title": req.body.title,
			"libraryDocument": req.body.libraryDocument,
			"category": req.body.category
		}];
		collection.insert(itemData, {safe:true}, function(err, result) {
			if (err){
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("fail...");
				console.log(err);
				res.end();
			} else {
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("ok...");
				res.end();
			}
		});
	});
};

exports.libraryUpdateItem = function(req, res) {
	req.db.collection('library', function(err, collection) {
		collection.update({_id: new mongo.ObjectID(req.body._id)}, {$set: { title: req.body.title, libraryDocument: req.body.libraryDocument, category: req.body.category } }, {safe:true}, function(err, result) {
			if (err){
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("fail...");
				console.log(err);
				res.end();
			} else {
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("ok...");
				res.end();
			}
		});
	});

};

exports.libraryDeleteItem = function(req, res) {
	req.db.collection('library', function(err, collection) {
		collection.remove({_id: new mongo.ObjectID(req.body.id)}, {safe:true}, function(err, result) {
			if (err){
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("fail...");
				console.log(err);
				res.end();
			} else {
                /*
                collection.find({_id: req.body.id}).toArray(function(err, library) {
                    console.log(err, library);
                  var filename = library[0].libraryDocument.split('/');
                  s3.delete(filename[filename.length-2], filename[filename.length-1])
                });
                */
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("ok...");
				res.end();
			}
		});
	});
};

/*
Get all content for 'Library Categories' section
*/
exports.libraryCategories = function(req, res) {
	req.db.collection('libraryCategories', function(err, collection) {

		collection.find().toArray(function(err, items) {
			
			res.jsonp(items);

		});

	});

};

exports.libraryCategoriesNewItem = function(req, res) {
	console.log('REQ');
	req.db.collection('libraryCategories', function(err, collection) {
		var itemData = [{
			"title": req.body.title
		}];
		collection.insert(itemData, {safe:true}, function(err, result) {
			console.log(result)
			if (err){
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("fail...");
				console.log(err);
				res.end();
			} else {
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("ok...");
				res.end();
			}
		});
	});

};

exports.libraryCategoriesUpdateItem = function(req, res) {
	req.db.collection('libraryCategories', function(err, collection) {
		collection.update({_id: new mongo.ObjectID(req.body.id)}, {$set: { title: req.body.title} }, {safe:true}, function(err, result) {
			if (err){
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("fail...");
				console.log(err);
				res.end();
			} else {
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("ok...");
				res.end();
			}
		});
	});

};

exports.libraryCategoriesDeleteItem = function(req, res) {
	req.db.collection('libraryCategories', function(err, collection) {
		collection.remove({_id: new mongo.ObjectID(req.body.id)}, {safe:true}, function(err, result) {
			if (err){
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("fail...");
				console.log(err);
				res.end();
			} else {
				res.writeHead(200, {"Content-Type":"text/plain"});
				res.write("ok...");
				res.end();
			}
		});
	});

};
