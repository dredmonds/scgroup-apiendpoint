//MongoDB
var mongo = require('mongodb');
var fs = require('fs');
var path = require('path');
var moment = require('moment');

var MongoClient = require('mongodb').MongoClient;
var Server = require('mongodb').Server;
var ObjectID = require('mongodb').ObjectID;
var db;

function fixId(result) {
    for (i in result) {
        result[i].id = result[i]._id;
        delete result[i]._id;
    }
    return result;
}
module.exports = function(args){
    var route = args.express();
        
    //Get a list of quiz   
    route.get('/all', function(req, res){
        req.db.collection('quiz.quiz').find({}).toArray(function(err, result) {
            result = fixId(result);
            res.send(result);
        });
    });

    //Get all info for a specific quiz
    route.get('/:id', function(req, res){
        var response = {};
        req.db.collection('quiz.quiz').find({_id: ObjectID(req.params.id)}).toArray(function(err, result) {
            if (err) throw err;
            result = fixId(result);
            response.info = [];
            response.info = result;

            req.db.collection('quiz.question').find({quiz: req.params.id}).toArray(function(err, result) {
                if (err) throw err;
                result = fixId(result);
                response.questions = [];
                response.questions = result;
                var query = [];
                for (i in result) {
                    query.push(result[i].id+"");
                }
                console.log(query);
                req.db.collection('quiz.question.answer').find({ quiz_question: {$in: query } }).toArray(function(err, result) {
                    if (err) throw err;
                    result = fixId(result);
                    response.answers = [];
                    response.answers = result;
                    res.send(response);
                });
            });
        });
    });
    
    //Delete a quiz
    route.delete('/:id', function(req, res){
        req.db.collection('quiz.quiz').remove({_id: ObjectID(req.params.id)}, function(err) {
            if (err) throw err;
                req.db.collection('quiz.question').find({quiz: req.params.id}).toArray(function(err, questions) {
                    if (err) throw err;
                    var query = [];
                    if (questions.length == 0) {
                        return res.status(200).send(true);
                    }
                    for (i in questions) {
                        query.push({quiz_question: questions[i]._id});
                    }
                    if (query.length > 1) {
                        query = {$or: query};
                    }
                    req.db.collection('quiz.question.answer').remove(query, function(err) {
                        if (err) throw err;
                        req.db.collection('quiz.question').remove({quiz: req.params.id}, function(err) {
                            if (err) throw err;
                            res.status(200).send(true);
                        });
                    });

                });
        });
    });

    //Get responses
    route.get('/analytics/:id', function(req, res){
        req.db.collection('quiz.response').find({quiz: req.params.id}).toArray(function(err, rows) {
            if (err) throw err;
            /*
            for (i in rows) {
                var fix = JSON.stringify(rows[i].answer).replace(/\\/g, '').substring(3).slice(0,-3);
                fix = '"[' + fix + ']"';
                rows[i].answer = fix;
            }
            */
            res.send(rows);
        });

    
    });

    //Get answered quizes by specific user
    route.get('/response/:user', function(req, res){
        req.db.collection('quiz.response').find({user: req.params.user}).toArray(function(err, rows) {
            if (err) throw err;
            res.send(rows);
        });

    
    });

    //Insert response
    route.post('/response', function(req, res){
        var date = new moment().format('DD-MM-YYYY HH:mm:ss');
        req.db.collection('quiz.quiz').find({_id: ObjectID(req.body.quiz)}).toArray(function(err, rows) {
            if (err) throw err;
            var count;
            if (rows[0].count) {
                count = rows[0].count + 1;
            } else {
                count = 1;
            }
            console.log(count);
            req.db.collection('quiz.quiz').update({_id: ObjectID(req.body.quiz)},{ $set:{ count: count } }, function(err, result) {
                if (err) throw err;
            });
        });

        req.db.collection('quiz.response').find({user: req.body.user, quiz: req.body.quiz}).toArray(function(err, result) {
            if (result.length >0) {
                req.db.collection('quiz.response').update({user: req.body.user, quiz: req.body.quiz},{ $set:{ user: req.body.user, firstname: req.body.firstname, lastname: req.body.lastname, answer: req.body.answer, points: req.body.points, time: req.body.time, date: date } }, function(err, result) {
                    res.status(200).send();
                });
            } else {
                req.db.collection('quiz.response').insert({user: req.body.user, firstname: req.body.firstname, lastname: req.body.lastname, quiz: req.body.quiz, answer: req.body.answer, points: req.body.points, time: req.body.time, date: date}, function(err, result) {
                    res.status(200).send();
                });
            }
        });
    
    });


    //Add a quiz
    route.post('/', function(req, res){
        req.db.collection('quiz.quiz').insert({name: req.body.name, type: req.body.type, visible: 0, published: 0, count: 0}, function(err, result) {
            if (err) throw err;
            res.send(result);
        });
    });

    //Update a quiz
    route.put('/:id', function(req, res){
        req.db.collection('quiz.quiz').update({_id: ObjectID(req.params.id)},{ $set:{ name: req.body.name, visible: req.body.visible, published: req.body.published } }, function(err, result) {
            if (err) throw err;
            res.send(result);
        });
    });

    //Add a question
    route.post('/question', function(req, res){
        req.db.collection('quiz.question').insert({name: req.body.name, quiz: req.body.quiz, type: req.body.type}, function(err, result) {
            if (err) throw err;
            result = {insertId: result.insertedIds[0]};
            res.send(result);
        });
    });

    //Update a question
    route.put('/question/:id', function(req, res){
        req.db.collection('quiz.question').update({_id: ObjectID(req.params.id)},{ $set:{ name: req.body.name, quiz: req.body.quiz, type: req.body.type } }, function(err, result) {
            if (err) throw err;
            res.send(result);
        });
    });

    //Remove a question
    route.delete('/question/:id', function(req, res){
        req.db.collection('quiz.question').remove({_id: ObjectID(req.params.id)}, function(err) {
            req.db.collection('quiz.question.answer').remove({quiz_question: req.params.id}, function(err) {
                res.send('true');
            });
        });
    });

    //Add an answer
    route.post('/answer', function(req, res){
        req.db.collection('quiz.question.answer').insert({quiz_question: req.body.quiz_question, value: req.body.value, correct: req.body.correct}, function(err, result) {
            res.send(result);
        });
    });

    //Update an answer
    route.put('/answer/:id', function(req, res){
        req.db.collection('quiz.question.answer').update({_id: ObjectID(req.params.id)},{ $set:{ quiz_question: req.body.quiz_question, value: req.body.value, correct: req.body.correct } }, function(err, result) {
            if (err) throw err;
            res.send(result);
        });
    });

    //Remove an answer
    route.delete('/answer/:id', function(req, res){
        req.db.collection('quiz.question.answer').find({_id: ObjectID(req.params.id)}).remove(function(err) {
            res.send('true');
        });
    });

    args.app.use('/quiz', route);

}
