//MongoDB
var mongo = require('mongodb');
var fs = require('fs');
var path = require('path');
var moment = require('moment');
var q = require('q');

var MongoClient = require('mongodb').MongoClient;
var Server = require('mongodb').Server;
var ObjectID = require('mongodb').ObjectID;
var db;
var email = require('../lib/email.js');

function fixId(result) {
    for (i in result) {
        result[i].id = result[i]._id;
        delete result[i]._id;
    }
    return result;
}
module.exports = function(args){
    var route = args.express();
        
    //Get a list of quiz   
    route.get('/all', function(req, res){
        req.db.collection('support.quiz').find({}).toArray(function(err, result) {
            result = fixId(result);
            res.send(result);
        });
    });

    //Get all info for a specific quiz
    route.get('/:id', function(req, res){
        var response = {};
        req.db.collection('support.quiz').find({_id: ObjectID(req.params.id)}).toArray(function(err, result) {
            if (err) throw err;
            result = fixId(result);
            response.info = [];
            response.info = result;

            req.db.collection('support.question').find({quiz: req.params.id}).toArray(function(err, result) {
                if (err) throw err;
                result = fixId(result);
                response.questions = [];
                response.questions = result;
                var query = [];
                for (i in result) {
                    query.push(result[i].id+"");
                }
                console.log(query);
                req.db.collection('support.question.answer').find({ quiz_question: {$in: query } }).toArray(function(err, result) {
                    if (err) throw err;
                    result = fixId(result);
                    response.answers = [];
                    response.answers = result;
                    res.send(response);
                });
            });
        });
    });
    
    //Delete a quiz
    route.delete('/:id', function(req, res){
        req.db.collection('support.quiz').remove({_id: ObjectID(req.params.id)}, function(err) {
            if (err) throw err;
                req.db.collection('support.question').find({quiz: req.params.id}).toArray(function(err, questions) {
                    if (err) throw err;
                    var query = [];
                    for (i in questions) {
                        query.push({quiz_question: questions[i]._id});
                    }
                    var mQuery = {_id: 0};
                    if (query.length > 0) mQuery = {$or: query};
                    req.db.collection('support.question.answer').remove(mQuery, function(err) {
                        if (err) throw err;
                        req.db.collection('support.question').remove({quiz: ObjectID(req.params.id)}, function(err) {
                            if (err) throw err;
                            res.status(200).send(true);
                        });
                    });

                });
        });
    });
    function match(id, arr) {
        for (i in arr) {
            if (arr[i]._id == id) {
                return arr[i];
            }
        }
        return id;
    }
    function getSupportResponses(req, _id) {
        var deferred =  q.defer();
        var query;
        if (_id) {
            query = {_id: ObjectID(_id)};
        } else if (req.params.id == 'all') {
            query = {};
        } else {
            query = {quiz: req.params.id};
        }
        req.db.collection("user").find({}).toArray(function(err, Users) {
            if (err) throw err;
            req.db.collection("support.question").find({}).toArray(function(err, Questions) {
                if (err) throw err;
                req.db.collection("support.question.answer").find({}).toArray(function(err, Answers) {
                    if (err) throw err;
                    req.db.collection("support.quiz").find({}).toArray(function(err, Quizs) {
                        if (err) throw err;
                        req.db.collection("support.response").find(query).toArray(function(err, Responses) {
                            if (err) throw err;
                            for (i in Responses) {
                                (function loop(index) {
                                    Responses[index].quiz = match(Responses[index].quiz, Quizs);
                                    Responses[index].user = match(Responses[index].user, Users);
                                    var answer = JSON.parse(Responses[index].answer);
                                    for (x in answer) {
                                        answer[x].question = match(answer[x].question, Questions);
                                        if (typeof answer[x].answer == 'object') {
                                            var tmp = answer[x].answer;
                                            for (y in tmp) {
                                                tmp[y] = match(tmp[y], Answers);
                                            }
                                            answer[x].answer = tmp;
                                        } else {
                                            answer[x].answer = match(answer[x].answer, Answers);
                                        }
                                    }
                                    Responses[index].answer = answer;
                                    if (index == Responses.length-1) {
                                        deferred.resolve(Responses);
                                    }
                                })(i);
                            }
                        });
                    });
                });
            });
        });
        return deferred.promise;
    }
    //Get responses
    route.get('/analytics/:id', function(req, res){
            getSupportResponses(req).then(function(result) {
                res.status(200).send(result);
            });
    });

    //Get answered quizes by specific user
    route.get('/response/:user', function(req, res){
        req.db.collection('support.response').find({user: req.params.user}).toArray(function(err, rows) {
            if (err) throw err;
            res.send(rows);
        });

    
    });

    //Insert response
    route.post('/response', function(req, res){
        var date = new moment().format('DD-MM-YYYY HH:mm:ss');
        req.db.collection('support.quiz').find({_id: ObjectID(req.body.quiz)}).toArray(function(err, rows) {
            if (err) throw err;
            var quiz = rows[0];
            var count = rows[0].count + 1;
            req.db.collection('support.quiz').update({_id: ObjectID(req.body.quiz)},{ $set:{ count: count } }, function(err, result) {
                if (err) throw err;
                req.db.collection('support.response').insert({user: req.body.user, quiz: req.body.quiz, answer: req.body.answer, points: req.body.points, time: req.body.time, date: date}, function(err, result) {
                var id = result.insertedIds[0];
                getSupportResponses(req, id).then(function(result) {
                    var template = fs.readFileSync(path.join(__dirname, '../email/support.html'), { encoding: 'utf8' });
                    query = '{{user.name}}';
                    template = template.replace(new RegExp(query, 'g'), result[0].user.first_name + ' ' + result[0].user.last_name);
                    query = '{{user.email}}';
                    template = template.replace(new RegExp(query, 'g'), result[0].user.username);
                    query = '{{support.name}}';
                    template = template.replace(new RegExp(query, 'g'), result[0].quiz.name);
                    query = '{{support.date}}';
                    template = template.replace(new RegExp(query, 'g'), result[0].date);

                    var response = '';

                    for (i in result[0].answer) {
                        response = response + '<h3>' + result[0].answer[i].question.name + '</h3>';
                        if (Array.isArray(result[0].answer[i].answer) ) {
                            for (x in result[0].answer[i].answer) {
                                response = response + '<p>' + result[0].answer[i].answer[x].value + '</p>';
                            }
                        } else if (typeof result[0].answer[i].answer == 'string') {
                            response = response + '<p>' + result[0].answer[i].answer + '</p>';
                        } else {
                            response = response + '<p>' + result[0].answer[i].answer.value + '</p>';
                        }
                    }

                    query = '{{response}}';
                    template = template.replace(new RegExp(query, 'g'), response);
                    email.send(quiz.email, result[0].quiz.name + ' - ' + req.config.name, template, req.config);
                    res.status(200).send();
                });
            });
        });

    
        });
    });
    //Update a response
    route.put('/response', function(req, res){
        req.db.collection('support.response').update({_id: ObjectID(req.body._id)},{ $set:{ points: req.body.points} }, function(err, result) {
            if (err) throw err;
            res.send(result);
        });
    });

    //Add a quiz
    route.post('/', function(req, res){
        req.db.collection('support.quiz').insert({name: req.body.name, type: req.body.type, visible: 0, published: 0, count: 0, email: req.body.email}, function(err, result) {
            if (err) throw err;
            res.send(result);
        });
    });

    //Update a quiz
    route.put('/:id', function(req, res){
        req.db.collection('support.quiz').update({_id: ObjectID(req.params.id)},{ $set:{ name: req.body.name, visible: req.body.visible, published: req.body.published, email: req.body.email } }, function(err, result) {
            if (err) throw err;
            res.send(result);
        });
    });

    //Add a question
    route.post('/question', function(req, res){
        req.db.collection('support.question').insert({name: req.body.name, quiz: req.body.quiz, type: req.body.type}, function(err, result) {
            if (err) throw err;
            result = {insertId: result.insertedIds[0]};
            res.send(result);
        });
    });

    //Update a question
    route.put('/question/:id', function(req, res){
        req.db.collection('support.question').update({_id: ObjectID(req.params.id)},{ $set:{ name: req.body.name, quiz: req.body.quiz, type: req.body.type } }, function(err, result) {
            if (err) throw err;
            res.send(result);
        });
    });

    //Remove a question
    route.delete('/question/:id', function(req, res){
        req.db.collection('support.question').remove({_id: ObjectID(req.params.id)}, function(err) {
            req.db.collection('support.question.answer').remove({quiz_question: req.params.id}, function(err) {
                res.send('true');
            });
        });
    });

    //Add an answer
    route.post('/answer', function(req, res){
        req.db.collection('support.question.answer').insert({quiz_question: req.body.quiz_question, value: req.body.value, correct: req.body.correct}, function(err, result) {
            res.send(result);
        });
    });

    //Update an answer
    route.put('/answer/:id', function(req, res){
        req.db.collection('support.question.answer').update({_id: ObjectID(req.params.id)},{ $set:{ quiz_question: req.body.quiz_question, value: req.body.value, correct: req.body.correct } }, function(err, result) {
            if (err) throw err;
            res.send(result);
        });
    });

    //Remove an answer
    route.delete('/answer/:id', function(req, res){
        req.db.collection('support.question.answer').find({_id: ObjectID(req.params.id)}).remove(function(err) {
            res.send('true');
        });
    });

    args.app.use('/support', route);

}
