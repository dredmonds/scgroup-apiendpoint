var ObjectID = require('mongodb').ObjectID;

module.exports = function(args){
    var route = args.express();

    route.get('/:id', function(req, res){
        req.db.collection('terms').find({id: req.params.id}).toArray(function(err, result) {
            res.status(200).send(result[0]);
        });
    });

    route.post('/', function(req, res) {
        req.db.collection('terms').insert({id: req.body.id, link: req.body.link}, function(err, result) {
            if (err) throw err;
            res.status(200).send();
        });
    });

    route.put('/', function(req, res) {
        req.db.collection('terms').update({id: req.body.id},{ $set: {link: req.body.link} }, function(err, result) {
            if (err) throw err;
            res.status(200).send();
        });
    });

    route.delete('/:id', function(req, res) {
        req.db.collection('terms').remove({id: req.params.id}, function(err) {
            if (err) throw err;
            res.status(200).send();
        });
    });

    args.app.use('/terms', route);

}
