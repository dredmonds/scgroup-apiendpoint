var aws = require('aws-sdk');
var Mailgen = require('mailgen');
var q = require('q');
var ses = new aws.SES(require('../amazon.json'));
var serverConfig = require('../config.json');
var Log = require('./log');

exports.send = function(to, subject, body, client) {
    var deferred =  q.defer();

    var mailGenerator = new Mailgen({
        theme: 'default',
        product: {
            name: 'The StaffConnect Team',
            link: 'http://staffconnectapp.com/',
            logo: 'https://' + serverConfig.url + '/api/assets/logo.png',
            copyright: 'Copyright &copy; <a href="http://www.staffconnectapp.com/">StaffConnect</a>. All rights reserved.'
        }
    });

    var email = {
      body: body
    };

    var eParams = {
        Destination: {
            ToAddresses: [to]
        },
        Message: {
            Body: {
              Html: {
                  Data: mailGenerator.generate(email)
              },
              Text: {
                Data: mailGenerator.generatePlaintext(email)
              }
            },
            Subject: {
                Data: subject
            }
        },
        Source: "StaffConnect <noreply@staffconnectapp.com>"
    };

    var email = ses.sendEmail(eParams, function(err, result){
        Log.log(client.database.name, {tag: 'EMAIL', to: to, subject: subject, body: body, err: err, response: result});
        if (err) return deferred.reject(err);
        deferred.resolve(result);
    });

    return deferred.promise;
}
