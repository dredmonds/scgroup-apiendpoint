exports.register = function(params) {
  /*
  var paramsExample = {
    user: {
      username: '',
      password: '',
      first_name: '',
      last_name: ''
    },
    config: req.config,
    password: '' //will be sent in plain text to user!
  }
  */

  var body = {
      name: params.user.first_name + ' ' + params.user.last_name,
      intro: ['Welcome to your new ' + params.config.name + ' StaffConnect App.', '<h2>Step 1: </h2>Download the StaffConnect App onto your mobile device from either the Apple App Store or Google Play store.', '<h2>Step 2: </h2>To confirm your account please tap the large button below or click the link: <a href="' + 'https://' + params.config.url + '/api/verify/' + params.user.username + '/' + params.pin + '?pin=' + params.config.pin + '">' + 'https://' + params.config.url + '/api/verify/' + params.user.username + '/' + params.pin + '?pin=' + params.config.pin + '</a>'],
      action: {
          button: {
              color: '#22BC66',
              text: 'Confirm your account',
              link: 'https://' + params.config.url + '/api/verify/' + params.user.username + '/' + params.pin + '?pin=' + params.config.pin
          }
      },
      outro: ['<h2>Step 3: </h2>Open the StaffConnect App and enter the organisation ID: <b>' + params.config.pin + '</b>'],
      greeting: 'Hi ',
      signature: 'Thank you'
  };
  if (params.password) {
      body.outro.push('<h2>Step 4: </h2>Log in using your email and this temporary password: <b>' + params.password + '</b>');
  }
  body.outro.push('If you have any problems, please contact <a href="mailto: support@staffconnectapp.com">support@staffconnectapp.com</a> for assistance.');

  return body;
}
