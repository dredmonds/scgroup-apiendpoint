var aws = require('../amazon.json');
var lawgs = require('lawgs');
var serverConfig = require('../config.json');

exports.log = function(name, data) {
    lawgs.config({
        aws: aws
    });
    var logger  = lawgs.getOrCreate(serverConfig.bucket);
    logger.log(name, data);
}
