exports.restrict = function(req, res, next) {
    var token = req.headers.token;
    req.db.collection('session').find({token: token}).toArray(function(err, data) {
        if (data.length == 0) return res.status(401).send('Invalid Session.');
        req.db.collection('user').find({_id: data[0].userId}).toArray(function(err, result) {
            if (result.length == 0) return res.status(401).send('User doesn\'t exist.');
            if (result[0].disabled == "true") {
                return res.status(401).send('Account disabled.');
            }
            req.user = result[0];
            var update = {
              lastActive: new Date()
            }
            req.db.collection('user').update({_id: req.user._id}, {$set: update});
            req.log.user = req.user;
            next();
        });
    });
}
exports.admin = function(req, res, next) {
    if (req.user.role == 'admin') {
        next();
    } else {
        return res.status(401).send('You must be an admin to use this feature.');
    }
}
exports.sc = function(req, res, next) {
    var domain = req.user.username.split('@')[1];
    if (domain == 'staffconnectapp.com') {
        next();
    } else {
        return res.status(401).send('Must be StaffConnect Staff to access this feature.');
    }
}
