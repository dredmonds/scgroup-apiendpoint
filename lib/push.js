var AWS = require('aws-sdk');
var q = require('q');
var fs = require('fs');
var path = require('path');
var uuid = require('node-uuid');
var Log = require('./log');
var config = require('../amazon.json');

config.region = 'eu-west-1';
AWS.config.update(config);

var sns = new AWS.SNS();

var arn = {
  GCM: 'arn:aws:sns:eu-west-1:627527387828:app/GCM/StaffConnect_Android',
  APNS: 'arn:aws:sns:eu-west-1:627527387828:app/APNS/StaffConnect_iOS'
}

exports.registerUser = function(user, token, client, platform) {
    var deferred =  q.defer();

    sns.createPlatformEndpoint({
        PlatformApplicationArn: arn[platform],
        Token: token,
        CustomUserData: client.database.name + '_' + user._id.toString() + '_' + user.username.toString()
    }, function(err, data) {
        Log.log(client.database.name, {tag: 'PUSH REGISTER', user: user, token: token, platform: platform, error: err, result: data});
        if (err) return deferred.reject(err);
        deferred.resolve(data.EndpointArn);
    });

    return deferred.promise;
}

exports.unregisterUser = function(user, client) {
  var deferred = q.defer();

  if (!user.arn) {
    deferred.reject('User not registered on push.');
  } else {
    var params = {
          EndpointArn: user.arn
    };
    sns.deleteEndpoint(params, function(err, data) {
        Log.log(client.database.name, {tag: 'PUSH UNREGISTER', user: user, error: err, result: data});
        if (err) return deferred.reject(err);
        deferred.resolve(data);
    });
  }

  return deferred.promise;
}


exports.sendPush = function(user, message, type, postId, client) {
  var deferred =  q.defer();

  if (!user.arn) deferred.reject('User not registered on push.');

  var id = Math.random() * 999999999;
  id = ~~id;
  var payload = {
      "GCM": "{ \"data\": { \"notId\": \"" + id + "\", \"title\": \"StaffConnect\", \"badge\": 1, \"sound\": \"default\", \"message\": \"" + message + "\", \"type\": \"" + type + "\", \"id\": \"" + postId + "\" } }",
      "APNS": "{ \"aps\": {\"notId\": \"" + id + "\", \"title\": \"StaffConnect\", \"badge\": 1, \"sound\": \"default\", \"alert\": \"" + message + "\", \"type\": \"" + type + "\", \"id\": \"" + postId + "\" } }",
      "APNS_SANDBOX": "{ \"aps\": {\"notId\": \"" + id + "\", \"title\": \"StaffConnect\", \"badge\": 1, \"sound\": \"default\", \"alert\": \"" + message + "\", \"type\": \"" + type + "\", \"id\": \"" + postId + "\" } }"
  }
  payload = JSON.stringify(payload);

  sns.publish({
      Message: payload,
      MessageStructure: 'json',
      TargetArn: user.arn
  }, function(err, data) {
      Log.log(client.database.name, {tag: 'PUSH SEND', user: user, message: message, type: type, error: err, result: data, payload: payload});
      if (err) return deferred.reject(err);
      deferred.resolve(data);
  });

  return deferred.promise;
}
