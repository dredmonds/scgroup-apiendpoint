var AWS = require('aws-sdk');
var config = require('../amazon.json');
var fs = require('fs');
var serverConfig = require('../config.json');

config.region = 'eu-west-1';
var s3 = new AWS.S3(config);

var q = require('q');

exports.upload = function(bucket, key, file) {
    var deferred =  q.defer();

    s3.createBucket({Bucket: serverConfig.bucket}, function() {
      var params = {Bucket: serverConfig.bucket, Key: bucket + '/' + key, Body: file, ACL:'public-read'};
      s3.putObject(params, function(err, data) {
          console.log(err, data);
        if (err) return deferred.reject(err);
        deferred.resolve(data);
      });

  });

  return deferred.promise;
}

exports.deleteFromBucket = function(bucket, folder, key) {
    var deferred =  q.defer();

    s3.deleteObjects({
        Bucket: bucket,
        Delete: {
            Objects: [
                { Key: folder + '/' + key }
            ]
        }
    }, function(err, data) {
        if (err) return deferred.reject(err);
        deferred.resolve(data);
    });

    return deferred.promise;
}

exports.delete = function(folder, key) {
    return exports.deleteFromBucket(serverConfig.bucket, folder, key);
}

exports.deleteFolderFromBucket = function(bucket, folder) {
    var params = {
        Bucket: bucket,
        Prefix: folder
    };

    s3.listObjects(params, function(err, data) {
        params = {Bucket: bucket};
        params.Delete = {Objects:[]};

        data.Contents.forEach(function(content) {
            params.Delete.Objects.push({Key: content.Key});
        });

        s3.deleteObjects(params, function(err, data) {
            if (err) {
                console.error(err);
            }
        });
    });
}
