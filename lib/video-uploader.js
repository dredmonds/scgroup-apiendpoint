var aws = require('aws-sdk');
var amazonConfig = require('../amazon.json');
var transcoderConfig = require('../transcoder.json');
var config = require('../config.json');
var fs = require('fs');

transcoderConfig.s3BucketIn = config.bucket;
transcoderConfig.s3BucketOut = config.bucket;

var s3 = new aws.S3(amazonConfig);
var eltr = new aws.ElasticTranscoder(amazonConfig);

/*
    Options is an object with the following properties:
    options.inputFilePath
    options.rawVideoKey
    options.outputVideoDir
    options.outputVideoName
    options.s3BucketIn
    options.pipelineId
    options.presets
*/
module.exports.uploadVideoToS3 = function (options) {
    var self = this;

    var s3Params = {
        Bucket: options.s3BucketIn,
        Key: options.rawVideoKey,
        Body: fs.createReadStream(options.inputFilePath)
    };

    return s3.upload(s3Params).promise()
    .then(function (result) {
        if (!options.keepLocalUploadedFile) {
            fs.unlinkSync(options.inputFilePath);
        }
        return self.transcodeVideoInS3({
            rawVideoKey: s3Params.Key,
            outputVideoDir: options.outputVideoDir,
            outputVideoName: options.outputVideoName,
            userMetadata: options.userMetadata,
            pipelineId: options.pipelineId,
            presets: options.presets
        });
    })
    .catch(function (err) {
        throw new Error('Failed to upload video to S3. ' + err.message);
    });
};

module.exports.transcodeVideoInS3 = function (options) {
    var params = {
        PipelineId: options.pipelineId,
        OutputKeyPrefix: options.outputVideoDir,
        Input: {
            Key: options.rawVideoKey,
            FrameRate: 'auto',
            Resolution: 'auto',
            AspectRatio: 'auto',
            Interlaced: 'auto',
            Container: 'auto'
        },
        Outputs: [],
        UserMetadata: {
            date: String(Date.now()),
            copyright: options.copyright || ''
        },
        Playlists: [{
            Format: 'HLSv4',
            Name: options.outputVideoName,
            OutputKeys: []
        }]
    };

    options.presets.forEach(function (preset) {
        var outputOptions = {
            Key: options.outputVideoName + preset.suffix,
            PresetId: preset.id,
            SegmentDuration: preset.segmentDuration
        };
        if (preset.type == 'video') {
            outputOptions.ThumbnailPattern = options.outputVideoName + preset.suffix + '-thumbnail-{count}';
        }
        params.Outputs.push(outputOptions);
        params.Playlists[0].OutputKeys.push(outputOptions.Key);
    });

    if (options.userMetadata) {
        params.UserMetadata = options.userMetadata;
    }

    return eltr.createJob(params).promise()
    .catch(function (err) {
        throw new Error('Failed to transcode ' + options.rawVideoKey + ' video with AWS ElasticTranscoder. ' + err.message);
    });
};
