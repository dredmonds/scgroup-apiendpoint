var ObjectID = require('mongodb').ObjectID;
var moment = require('moment');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var uuid = require('node-uuid');
var q = require('q');
var lwip = require('lwip');
var exec = require('child_process').exec;
var mmmagic = require('mmmagic');
var magic = new mmmagic.Magic(mmmagic.MAGIC_MIME_TYPE);
var mime = require('mime');
var sharp = require('sharp');
var push = require('../lib/push');
var bcrypt = require('bcrypt');
var generatePassword = require('password-generator');
var email = require('../lib/email');
var csvArray = require("csv-to-array");
var s3 = require('../lib/s3');
var serverConfig = require('../config.json');
var email_builder = require('../lib/email_builder');

function match(id, arr) {
    for (i in arr) {
        if (arr[i]._id.toString() == id) {
            return arr[i];
        }
    }
    return id;
}

function checkPass(password) {
    if (password.length < 8) return 'Password must be 8 characters in length.';
    return true;
}


exports.users = {
    get: function(req, res) {
        req.db.collection("permissions").find({}).toArray(function(err, result) {
            if (err) return res.status(400).send(err);
            var permissions = result;
            req.db.collection("user").find({}).sort({first_name:1}).toArray(function(err, result) {
                if (err) return res.status(400).send(err);
                for (i in result) {
                    if (result[i].info && result[i].info.created_method == 'app_nondesk') delete result[i].username;
                    (function(index) {
                        result[index].location = match(result[index].location, permissions);
                        result[index].department = match(result[index].department, permissions);
                        if (index == result.length-1) {
                            res.status(200).send(result);
                        }
                    })(i);
                }
            });
        });
    },
    put: function(req, res) {
        var data = { first_name: req.body.first_name, last_name: req.body.last_name, location: req.body.location, department: req.body.department, role: req.body.role, disabled: req.body.disabled };
        if (req.body.pin) data.pin = req.body.pin;
        if (req.body.pin == null) data.pin = null;
        if (req.body.newPassword) {
            var password = req.body.newPassword;

            if (checkPass(password) !== true) {
                return res.status(400).send(checkPass(password));
            }
            data.password = bcrypt.hashSync(password, 10);
            data.tempPass = true;
        }

        req.db.collection("user").update({_id: ObjectID(req.body._id)}, { $set: data}, {upsert: true}, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    post: function(req, res) {
        console.log(req.body);
        if (!req.body.username || !req.body.first_name || !req.body.last_name) {
            return res.status(400).end('Information is missing.');
        }
        if (req.body.username.indexOf('@') == -1) {
            return res.status(400).send('Username must be an email address.');
        }
        var username = req.body.username.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
        req.db.collection("user").find({username: {$regex: new RegExp('^' + username + '$', 'i')}}).toArray(function(err, result) {
            if (result.length > 0) return res.status(400).send('That username is taken.');
            var data = {
                username: req.body.username,
                first_name: req.body.first_name,
                last_name: req.body.last_name,
                location: req.body.location,
                department: req.body.department,
                role: req.body.role || 'user',
                disabled: req.body.disabled || 'false',
                pin: null,
                password: req.body.password || generatePassword(10),
                created: new Date(),
                info: req.body.info || {}
            }
            var password = data.password;

            if (checkPass(data.password) !== true) {
                return res.status(400).send(checkPass(data.password));
            }

            if (!req.body.password) {
                data.pin = generatePassword(4);
            }
            if (!req.body.password) data.tempPass = true;
            data.password = bcrypt.hashSync(data.password, 10);

            req.db.collection("user").insert(data, function(err, result) {
                if (err) return res.status(400).send(err);
                if (data.pin != null) {
                    email.send(data.username, "Welcome to your new " + req.config.name + ' StaffConnect App!', email_builder.register({ user: req.body, config: req.config, pin: data.pin, password: password }), req.config )
                        .then(function(result) {
                            res.status(200).send('Success');
                        }, function(error) {
                            res.status(400).send(error);
                        });
                } else {
                    res.status(200).send('Success');
                }
            });

        });
    }
};

exports.locations = {
    get: function(req, res) {
        req.db.collection("permissions").find({type: 'location'}).toArray(function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    post: function(req, res) {
      var name = req.body.name.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
      req.db.collection("permissions").find( {name: {$regex: new RegExp('^' + name + '$', 'i')}, type: 'location'} ).toArray(function(err, result) {
        if (err) return res.status(400).send(err);
        if (result.length>0) return res.status(400).send('Location name already in use.');
        req.db.collection("permissions").insert({name: req.body.name, type: 'location'}, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
      });
    },
    put: function(req, res) {
      var name = req.body.name.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
      req.db.collection("permissions").find( {name: {$regex: new RegExp('^' + name + '$', 'i')}, type: 'location', _id: {$ne: ObjectID(req.body._id)} } ).toArray(function(err, result) {
        if (err) return res.status(400).send(err);
        if (result.length>0) return res.status(400).send('Location name already in use.');
        req.db.collection("permissions").update({_id: ObjectID(req.body._id)}, { $set:{ name: req.body.name } }, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
      });
    },
    delete: function(req, res) {
        req.db.collection("user").find({location: req.params.id}).count(function(err, result) {
            if (err) return res.status(400).send(err);
            if (result.length > 0) return res.status(400).send('Location contains users. Please remove the users and try again.');
            req.db.collection("permissions").remove({_id: ObjectID(req.params.id)}, function(err) {
                if (err) return res.status(400).send(err);
                res.status(200).send();
            });
        });
    }
}

exports.departments = {
    get: function(req, res) {
        req.db.collection("permissions").find({type: 'department'}).toArray(function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    post: function(req, res) {
      var name = req.body.name.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
      req.db.collection("permissions").find( {name: {$regex: new RegExp('^' + name + '$', 'i')}, type: 'department'} ).toArray(function(err, result) {
        if (err) return res.status(400).send(err);
        if (result.length>0) return res.status(400).send('Department name already in use.');
        req.db.collection("permissions").insert({name: req.body.name, type: 'department'}, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
      });
    },
    put: function(req, res) {
      var name = req.body.name.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
      req.db.collection("permissions").find( {name: {$regex: new RegExp('^' + name + '$', 'i')}, type: 'department', _id: {$ne: ObjectID(req.body._id)} } ).toArray(function(err, result) {
        if (err) return res.status(400).send(err);
        if (result.length>0) return res.status(400).send('Department name already in use.');
        req.db.collection("permissions").update({_id: ObjectID(req.body._id)}, { $set:{ name: req.body.name } }, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
      });
    },
    delete: function(req, res) {
        req.db.collection("user").find({department: req.params.id}).count(function(err, result) {
            if (err) return res.status(400).send(err);
            if (result > 0) return res.status(400).send('Department contains users. Please remove the users and try again.');
            req.db.collection("permissions").remove({_id: ObjectID(req.params.id)}, function(err) {
                if (err) return res.status(400).send(err);
                res.status(200).send();
            });
        });
    }
}

exports.communities = {
    get: function(req, res) {
        var query;
        if (req.user.role && req.user.role == 'admin') {
            query = {type: req.params.type};
        } else {
            query = { type: req.params.type, visible: true, $and: [{locations: req.user.location}, {'departments.department': req.user.department}] };
        }
        console.log(query);
        req.db.collection("communities").find(query).sort({name: 1}).toArray(function(err, result) {
            for (i in result) {
                for (ii in result[i].departments) {
                    if (result[i].departments[ii].department == req.user.department) {
                        result[i].myPermission = result[i].departments[ii].permissions;
                    }
                }
            }
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    post: function(req, res) {
        req.db.collection("communities").insert({name: req.body.name, departments: req.body.departments, locations: req.body.locations, type: req.params.type, visible: req.body.visible, moderation: req.body.moderation}, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    put: function(req, res) {
        console.log(req.body);
        req.db.collection("communities").update({_id: ObjectID(req.body._id)}, { $set:{ name: req.body.name, locations: req.body.locations, departments: req.body.departments, visible: req.body.visible, moderation: req.body.moderation } }, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    delete: function(req, res) {
        req.db.collection("communities").remove({_id: ObjectID(req.params.id)}, function(err) {
            if (err) return res.status(400).send(err);
            req.db.collection("communities.posts").find({community: req.params.id}).sort({date: -1}).toArray(function(err, posts) {
                if (err) return res.status(400).send(err);
                if (posts.length == 0) {
                    return res.status(200).send();
                }
                for (i in posts) {
                    (function(index) {
                        req.db.collection("communities.posts").remove({_id: posts[index]._id}, function(err) {
                            if (err) return res.status(400).send(err);
                            req.db.collection("communities.posts.likes").remove({post: posts[index]._id.str}, function(err) {
                                if (err) return res.status(400).send(err);
                                req.db.collection("communities.posts.comments").remove({post: posts[index]._id.str}, function(err) {
                                    if (err) return res.status(400).send(err);
                                    req.db.collection("communities.posts.subscribe").remove({post: posts[index]._id.str}, function(err) {
                                        if (err) return res.status(400).send(err);
                                        if (index == posts.length-1) {
                                            res.status(200).send();
                                        }
                                    });
                                });
                            });
                        });
                    })(i);
                }
            });
        });
    }
}

exports.posts = {
    get: function(req, res) {
        /*
        var query;
        if (req.query.user) {
            query = {author: req.query.user};
        } else if (req.query.post) {
            query = {_id: ObjectID(req.query.post)};
        } else if (req.query.search) {
            query = {$or: [
                {title: {$regex : ".*" + req.params.search + ".*"}},
                {content: {$regex : ".*" + req.params.search + ".*"}}
            ]};
        } else {
            var data = req.params.id.split(',');
            if (data.length == 1) {
                query = {community: req.params.id};
            } else {
                var or = [];
                for (i in data) {
                    var tmp = {community: data[i]};
                    or.push(tmp);
                }
                query = {$or: or};
            }
        }

        */

        var query = {
            $or: [],
            $and: [],
            author: false,
            _id: false,
            community: false
        };
        if (!req.query.videosReady) {
          query.videosReady = { $ne: false };
        }
        if (req.query.user) {
            query.author = req.query.user;
        }
        if (req.query.post) {
            query._id = ObjectID(req.query.post);
        }
        var search = [];
        if (req.query.search) {
            search.push({title: {$regex : ".*" + req.query.search + ".*", '$options' : 'i'}});
            search.push({content: {$regex : ".*" + req.query.search + ".*", '$options' : 'i'}})
        };

        var data = req.params.id.split(',');
        if (data.length == 1) {
            query.community = req.params.id;
        } else {
            for (i in data) {
                var tmp = {community: data[i]};
                query.$or.push(tmp);
            }
        }
        if (search.length == 0) {
            delete query.$and;
        } else {
            if (query.$or.length <= 1) {
                query.$or = search;
                delete query.$and;
            } else {
                query.$and = [{$or: query.$or}, {$or: search}];
                delete query.$or;
            }
        }
        if (query.$or && query.$or.length == 0) delete query.$or;
        if (query.author == false) delete query.author;
        if (query._id == false) delete query._id;
        if (query.community == false) delete query.community;
        console.log(JSON.stringify(query));

        var page = req.query.page || 0;
        var amount = req.query.amount || 10;
        req.db.collection("communities").find({}).toArray(function(err, communities) {
            if (err) return res.status(400).send(err);
            req.db.collection("communities.posts").find(query).sort({date: -1}).skip(page * amount).limit(amount * 1).toArray(function(err, posts) {
                if (err) return res.status(400).send(err);
                req.db.collection("user").find().toArray(function(err, users) {
                    if (err) return res.status(400).send(err);
                    for (i in users) {
                        if (users[i].info && users[i].info.created_method == 'app_nondesk') delete users[i].username;
                    }
                    req.db.collection("communities.posts.likes").find().toArray(function(err, likes) {
                        if (err) return res.status(400).send(err);
                        req.db.collection("communities.posts.comments").find().toArray(function(err, comments) {
                            if (err) return res.status(400).send(err);
                            req.db.collection("communities.posts.subscribe").find().toArray(function(err, subscribe) {
                                if (err) return res.status(400).send(err);
                                if (posts.length == 0) {
                                    res.status(200).send();
                                }
                                for (i in posts) {
                                    posts[i].likes = [];
                                    posts[i].comments = [];
                                    posts[i].subscribe = [];
                                    (function(index) {
                                        for (i in comments) {
                                            if (comments[i].post == posts[index]._id) {
                                                posts[index].comments.push(comments[i]);
                                            }
                                        }
                                        for (i in likes) {
                                            if (likes[i].post == posts[index]._id) {
                                                posts[index].likes.push(likes[i].user);
                                            }
                                        }
                                        for (i in subscribe) {
                                            if (subscribe[i].post == posts[index]._id) {
                                                posts[index].subscribe.push(subscribe[i].user);
                                            }
                                        }
                                        for (i in communities) {
                                            if (communities[i]._id.toString() == posts[index].community) {
                                                posts[index].communityInfo = communities[i];
                                            }
                                            for (ii in communities[i].departments) {
                                                if (communities[i].departments[ii].department == req.user.department) {
                                                    communities[i].myPermission = communities[i].departments[ii].permissions;
                                                }
                                            }
                                        }


                                        posts[index].author = match(posts[index].author, users);
                                        posts[index].actualAuthor = match(posts[index].actualAuthor, users);
                                        if (index == posts.length-1) {
                                            res.status(200).send(posts);
                                        }
                                    })(i);
                                }
                            });
                        });
                    });
                });
            });
        });
    },
    post: function(req, res) {
        var query = {
            title: req.body.title,
            content: req.body.content,
            status: req.body.status || 0,
            date: new Date(),
            updated: new Date(),
            community: req.body.community,
            attachments: req.body.attachments || [],
            author: req.body.author || req.user._id,
            actualAuthor: req.user._id
        };
        if (query.attachments.length > 0) {
            for (i in query.attachments) {
                if (query.attachments[i].type == 'video') query.videosReady = false;
            }
        }
        req.db.collection("communities.posts").insert(query, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    put: function(req, res) {
        function matchAttachment(filename, arr) {
            for (i in arr) {
                if (arr[i].filename == filename) {
                    return true;
                }
            }
            return false;
        }


        req.db.collection("communities.posts").find({_id: ObjectID(req.body._id)}).toArray(function(err, posts) {
            var post = posts[0];
            var query = {
                title: req.body.title,
                content: req.body.content,
                status: req.body.status || 0,
                updated: new Date(),
                community: req.body.community,
                attachments: req.body.attachments || [],
                author: req.body.author || req.user._id,
                actualAuthor: req.body.actualAuthor || req.user._id
            };
            if (req.body.videosReady) query.videosReady = req.body.videosReady;
            req.db.collection("communities.posts").update({_id: ObjectID(req.body._id)}, { $set: query }, function(err, result) {
                if (err) return res.status(400).send(err);
                res.status(200).send(result);
            });

        });
    },
    delete: function(req, res) {
        req.db.collection("communities.posts").find({_id: ObjectID(req.params.id)}).toArray(function(err, posts) {
            var post = posts[0];
            for (i in post.attachments) {
                var attachment = post.attachments[i];
                var filename = attachment.filename.split('/');
                if (attachment.type == 'video') {
                    // Delete the whole folder which contains all the HLS video chunks, thumbnails, etc.
                    s3.deleteFolderFromBucket(filename[3], filename.slice(4, filename.length - 1).join('/'));
                } else {
                    s3.delete(filename[filename.length-2], filename[filename.length-1])

                    // TODO: Can non-video files have a thumbnail and/or preview?
                    if (attachment.thumbname) {
                        var thumbname = attachment.thumbname.split('/');
                        s3.delete(thumbname[thumbname.length-2], thumbname[thumbname.length-1])
                    }
                    if (attachment.previewname) {
                        var previewname = attachment.previewname.split('/');
                        s3.delete(previewname[previewname.length-2], previewname[previewname.length-1])
                    }
                }
            }
            req.db.collection("communities.posts").remove({_id: ObjectID(req.params.id)}, function(err) {
                req.db.collection("communities.posts.likes").remove({post: req.params.id}, function(err) {
                    req.db.collection("communities.posts.comments").remove({post: req.params.id}, function(err) {
                        req.db.collection("communities.posts.subscribe").remove({post: req.params.id}, function(err) {
                            if (err) return res.status(400).send(err);
                            res.status(200).send();
                        });
                    });
                });
            });
        });
    }
}

exports.likes = {
    get: function(req, res) {
        req.db.collection("user").find().toArray(function(err, users) {
            if (err) return res.status(400).send(err);
            for (i in users) {
                if (users[i].info && users[i].info.created_method == 'app_nondesk') delete users[i].username;
            }
            req.db.collection("communities.posts.likes").find({post: req.params.id}).toArray(function(err, result) {
                if (err) return res.status(400).send(err);
                if (result.length == 0) {
                    res.status(200).send();
                }
                for (i in result) {
                    (function(index) {
                        result[index].user = match(result[index].user, users);

                        if (index == result.length-1) {
                            res.status(200).send(result);
                        }
                    })(i);
                }
            });
        });
    },
    post: function(req, res) {
        req.db.collection("communities.posts.likes").find({post: req.body.post, user: req.user._id}).toArray( function(err, result) {
            if (result.length == 0) {
                req.db.collection("communities.posts.likes").insert({post: req.body.post, user: req.user._id, date: new Date()}, function(err, result) {
                    if (err) return res.status(400).send(err);
                    req.db.collection("user").find({ arn: {$exists: true} }).toArray(function(err, users) {
                        req.db.collection("communities.posts").find({_id: ObjectID(req.body.post)}).toArray(function(err, Post) {
                            req.db.collection("communities.posts.subscribe").find({post: req.body.post, user: Post[0].actualAuthor}).toArray(function(err, subscribe) {
                                if (err) return res.status(400).send(err);
                                if (subscribe.length == 0) {
                                    res.status(200).send(result);
                                }
                                var title = Post[0].title;
                                for (i in subscribe) {
                                    (function(index) {
                                        for (i in users) {
                                            if (users[i]._id.toString() == subscribe[index].user.toString() && users[i]._id.toString() !== req.user._id.toString() ) {
                                                push.sendPush(users[i], req.user.first_name + ' ' + req.user.last_name + ' liked your post \\"' + title + '\\"', 'like', req.body.post, req.config)
                                            }
                                        }
                                        if (index == subscribe.length-1) {
                                            res.status(200).send(result);
                                        }
                                    })(i);
                                }
                            });
                        });
                    });
                });
            }
        });
    },
    delete: function(req, res) {
        req.db.collection("communities.posts.likes").remove({post: req.params.id, user: req.user._id}, function(err) {
            if (err) return res.status(400).send(err);
            res.status(200).send();
        });
    }
}

exports.comments = {
    get: function(req, res) {
        req.db.collection("user").find().toArray(function(err, users) {
            if (err) return res.status(400).send(err);
            for (i in users) {
                if (users[i].info && users[i].info.created_method == 'app_nondesk') delete users[i].username;
            }
            req.db.collection("communities.posts.comments").find({post: req.params.id}).toArray(function(err, result) {
                if (err) return res.status(400).send(err);
                if (result.length == 0) {
                    res.status(200).send();
                }
                for (i in result) {
                    (function(index) {
                        result[index].user = match(result[index].user, users);

                        if (index == result.length-1) {
                            res.status(200).send(result);
                        }
                    })(i);
                }
            });
        });
    },
    post: function(req, res) {
        req.db.collection("communities.posts.comments").insert({post: req.body.post, body: req.body.body, user: req.user._id, date:new Date(), updated: new Date()}, function(err, result) {
            if (err) return res.status(400).send(err);
			req.db.collection("user").find({ arn: {$exists: true} }).toArray(function(err, users) {
                req.db.collection("communities.posts.subscribe").find({post: req.body.post}).toArray(function(err, subscribe) {
                    req.db.collection("communities.posts").find({_id: ObjectID(req.body.post)}).toArray(function(err, Post) {
                        if (err) return res.status(400).send(err);
                        if (subscribe.length == 0) {
                            res.status(200).send(result);
                        }
                        var title = Post[0].title;
                        for (i in subscribe) {
                            (function(index) {
                                for (i in users) {
                                    if (users[i]._id.toString() == subscribe[index].user.toString() && users[i]._id.toString() !== req.user._id.toString() ) {
                                        push.sendPush(users[i], req.user.first_name + ' ' + req.user.last_name + ' commented on the post \\"' + title + '\\"', 'comment', req.body.post, req.config)
                                    }
                                }
                                if (index == subscribe.length-1) {
                                    res.status(200).send(result);
                                }
                            })(i);
                        }
                    });
                });
			});

        });
    },
    put: function(req, res) {
        req.db.collection("communities.posts.comments").update({_id: ObjectID(req.body._id)}, { $set:{ body: req.body.body, updated: new Date() } }, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    delete: function(req, res) {
        req.db.collection("communities.posts.comments").remove({_id: ObjectID(req.params.id)}, function(err) {
            if (err) return res.status(400).send(err);
            res.status(200).send();
        });
    }
}


function processImage(image, req) {
    var deferred =  q.defer();
    var source = fs.createReadStream(image.path);
    var extension = image.name.substr(image.name.lastIndexOf(".") + 1);
    var filename = req.config.database.name + '-' + uuid.v4() + '.' + extension;
    var thumbname = req.config.database.name + '-' + uuid.v4() + '-thumb.' + extension;
    var dest = fs.createWriteStream('uploads/' + filename);
    source.pipe(dest);

    gm('uploads/' + filename)
        .resize(150)
        .autoOrient()
        .write('uploads/' + thumbname, function (err) {
            if (err) return deferred.reject(err);
            deferred.resolve({filename: filename, thumbname: thumbname});
        });

    return deferred.promise;
}

exports.upload = function(req, res) {
    var files = [];
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        var filenames = [];
        var Files = [];
        for (i in files) {
            Files.push(files[i]);
        }
        var files = Files;
        for (i in files) {
            if (files[i].size > 1073741824) return res.status(400).send('Files must be under 1GB in size.');
        }
        for (i in files) {
            (function(index) {
                magic.detectFile(files[index].path, function(err, result) {
                      files[index].type = result;
                      files[index].name = 'filename.' + mime.extension(result);

                    if (files[index].type.indexOf('image') > -1) {
                        console.log(files[index]);
                        var id = uuid.v4();
                        var ext = files[index].name.substr(files[index].name.lastIndexOf(".") + 1);

                        var filename = id + '.' + ext;
                        var thumbname = id + '-thumb.' + ext;
                        var previewname = id + '-preview.' + ext;

                        var source = fs.createReadStream(files[index].path)
                        var dest = fs.createWriteStream(path.resolve(__dirname, '../uploads/' + filename))
                        source.pipe(dest)
                            .on('finish', function () {
                              sharp(path.resolve(__dirname, '../uploads/' + filename))
                                      .resize(1280, 720)
                                      .crop()
                                      .max()
                                      .toFile(path.resolve(__dirname, '../uploads/' + previewname), function(err) {
                                        if (err) return res.status(400).send(err);
                                        sharp(path.resolve(__dirname, '../uploads/' + filename))
                                                .resize(300, 300)
                                                .crop()
                                                .max()
                                                .toFile(path.resolve(__dirname, '../uploads/' + thumbname), function(err) {
                                                    if (err) return res.status(400).send(err);
                                                    s3.upload(req.config.database.name, filename, fs.createReadStream( path.resolve(__dirname, '../uploads/' + filename) ))
                                                      .then(function(result) {
                                                        fs.unlinkSync(path.resolve(__dirname, '../uploads/' + filename));
                                                        filename = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/' + filename;
                                                        s3.upload(req.config.database.name, thumbname, fs.createReadStream( path.resolve(__dirname, '../uploads/' + thumbname) ))
                                                          .then(function(result) {
                                                            fs.unlinkSync(path.resolve(__dirname, '../uploads/' + thumbname));
                                                            thumbname = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/' + thumbname;
                                                            s3.upload(req.config.database.name, previewname, fs.createReadStream( path.resolve(__dirname, '../uploads/' + previewname) ))
                                                              .then(function(result) {
                                                                fs.unlinkSync(path.resolve(__dirname, '../uploads/' + previewname));
                                                                previewname = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/' + previewname;
                                                                filenames.push({filename: filename, thumbname: thumbname, previewname: previewname, type: 'image', extension: ext});
                                                                if (index == files.length-1) {
                                                                    res.status(200).send(filenames);
                                                                }
                                                              }, function(err) {
                                                                throw err;
                                                                return res.status(400).send(err);
                                                              });
                                                          }, function(err) {
                                                            throw err;
                                                            return res.status(400).send(err);
                                                          });
                                                      }, function(err) {
                                                        throw err;
                                                        return res.status(400).send(err);
                                                      });

                                                });
                                      });

                            });


                    } else if (files[index].type.indexOf('video') > -1) {
                        var id = uuid.v4();
                        var ext = files[index].name.substr(files[index].name.lastIndexOf(".") + 1);

                        var filename = id + '.' + ext;
                        var thumbname = id + '-thumb.png';
                        var previewname = id + '-preview.png';
                        var thumbnameTemp = id + '-thumb-temp.png';

                        var source = fs.createReadStream(files[index].path);
                        var dest = fs.createWriteStream(path.resolve(__dirname, '../uploads/' + filename));
                        source.pipe(dest)
                            .on('finish', function () {

                                var cmd = 'ffmpeg -ss 1 -i ' + path.resolve(__dirname, "../uploads/" + filename) + ' -vf "crop=343:194" -vframes 1 ' + path.resolve(__dirname, "../uploads/" + thumbnameTemp) + ' -y';
                                exec(cmd, function(error, stdout, stderr) {
                                  sharp(path.resolve(__dirname, '../uploads/' + thumbnameTemp))
                                      .resize(1280, 720)
                                      .crop()
                                      .max()
                                      .toFile(path.resolve(__dirname, '../uploads/' + previewname), function(err) {
                                        if (err) return res.status(400).send(err);
                                        sharp(path.resolve(__dirname, '../uploads/' + thumbnameTemp))
                                            .resize(300, 300)
                                            .crop()
                                            .max()
                                            .toFile(path.resolve(__dirname, '../uploads/' + thumbname), function(err) {
                                                fs.unlinkSync(path.resolve(__dirname, '../uploads/' + thumbnameTemp));
                                                if (err) throw err;
                                                if (err) return res.status(400).send(err);
                                                s3.upload(req.config.database.name, filename, fs.createReadStream( path.resolve(__dirname, '../uploads/' + filename) ))
                                                  .then(function(result) {
                                                    fs.unlinkSync(path.resolve(__dirname, '../uploads/' + filename));
                                                    filename = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/' + filename;
                                                    s3.upload(req.config.database.name, thumbname, fs.createReadStream( path.resolve(__dirname, '../uploads/' + thumbname) ))
                                                      .then(function(result) {
                                                        fs.unlinkSync(path.resolve(__dirname, '../uploads/' + thumbname));
                                                        thumbname = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/' + thumbname;
                                                        s3.upload(req.config.database.name, previewname, fs.createReadStream( path.resolve(__dirname, '../uploads/' + previewname) ))
                                                          .then(function(result) {
                                                            fs.unlinkSync(path.resolve(__dirname, '../uploads/' + previewname));
                                                            previewname = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/' + previewname;
                                                            filenames.push({filename: filename, thumbname: thumbname, previewname: previewname, type: 'video', extension: ext});
                                                            if (index == files.length-1) {
                                                                res.status(200).send(filenames);
                                                            }
                                                          }, function(err) {
                                                              throw err;
                                                              return res.status(400).send(err);
                                                            });
                                                      }, function(err) {
                                                          throw err;
                                                          return res.status(400).send(err);
                                                        });
                                                    }, function(err) {
                                                      throw err;
                                                      return res.status(400).send(err);
                                                    });

                                            });
                                        });

                                });

                            });

                    } else {

                        var id = uuid.v4();
                        var ext = files[index].name.substr(files[index].name.lastIndexOf(".") + 1);

                        var filename = id + '.' + ext;

                        var source = fs.createReadStream(files[index].path)
                        var dest = fs.createWriteStream(path.resolve(__dirname, '../uploads/' + filename))


                        source.pipe(dest)
                            .on('finish', function () {
                                s3.upload(req.config.database.name, filename, fs.createReadStream( path.resolve(__dirname, '../uploads/' + filename) ))
                                  .then(function(result) {
                                    fs.unlinkSync(path.resolve(__dirname, '../uploads/' + filename));
                                    filename = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/' + filename;
                                    filenames.push({filename: filename, thumbname: thumbname, type: 'other', extension: ext});
                                    if (index == files.length-1) {
                                        res.status(200).send(filenames);
                                    }
                                  }, function(err) {
                                      throw err;
                                      console.log(err);
                                      return res.status(400).send(err);
                                    });
                            });
                    }
                });
            })(i);
        }
    });
}

exports.userDirectory = {
    get: function(req, res) {
        var filter = {
            username: 1,
            first_name: 1,
            last_name: 1,
            location: 1,
            department: 1,
            info: 1,
            avatar: 1,
            disabled: 1
        }
        var query;
        if (req.query.user) {
            query = {_id: ObjectID(req.query.user)};
        } else {
            query = {};
        }
        req.db.collection("permissions").find({}).toArray(function(err, result) {
            if (err) return res.status(400).send(err);
            var permissions = result;
            req.db.collection("user").find(query, filter).sort({last_name:1}).toArray(function(err, result) {
                if (err) return res.status(400).send(err);
                if (result.length == 0) return res.status(200).send(result);
                for (i in result) {
                    if (result[i].info && result[i].info.created_method == 'app_nondesk') delete result[i].username;
                    (function(index) {
                        result[index].location = match(result[index].location, permissions);
                        result[index].department = match(result[index].department, permissions);
                        if (index == result.length-1) {
                            res.status(200).send(result);
                        }
                    })(i);
                }
            });
        });

    }
};

exports.user = {
    put: function(req, res) {
        req.db.collection("user").update({_id: req.user._id}, { $set: { info: req.body.info} }, {upsert: true}, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    avatar: function(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
            for (first in files) break;
            var file = files[first];

            magic.detectFile(file.path, function(err, result) {
                file.type = result;
                file.name = req.user._id + '.' + mime.extension(result);

                var filename = file.name;
                if (file.type.indexOf('image') == -1) return res.status(400).send('File must be an image.');

                var source = fs.createReadStream(file.path);
                var dest = fs.createWriteStream(path.resolve(__dirname, '../uploads/' + filename));

                source.pipe(dest)
                    .on('finish', function () {
                        lwip.open(path.resolve(__dirname, '../uploads/' + filename), function(err, image){
                            if (err) return res.status(400).send(err);
                            image.batch()
                                .contain(150, 150)
                                .writeFile(path.resolve(__dirname, '../uploads/' + filename), function(err){
                                  s3.upload(req.config.database.name + '/avatar', filename, fs.createReadStream( path.resolve(__dirname, '../uploads/' + filename) ))
                                    .then(function(result) {
                                      fs.unlinkSync(path.resolve(__dirname, '../uploads/' + filename));
                                      filename = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/avatar/' + filename;
                                      req.db.collection("user").update({_id: ObjectID(req.user._id)}, { $set: { avatar: filename} }, {upsert: true}, function(err, result) {
                                          res.status(200).send(filename);
                                      });
                                  }, function(err) {
                                      throw err;
                                      return res.status(400).send(err);
                                    });
                                });
                        });

                    });
            });
        });
    }
}

exports.subscribe = {
    get: function(req, res) {
        req.db.collection("user").find().toArray(function(err, users) {
            if (err) return res.status(400).send(err);
            req.db.collection("communities.posts.subscribe").find({post: req.params.id}).toArray(function(err, result) {
                if (err) return res.status(400).send(err);
                if (result.length == 0) {
                    res.status(200).send();
                }
                for (i in result) {
                    (function(index) {
                        result[index].user = match(result[index].user, users);

                        if (index == result.length-1) {
                            res.status(200).send(result);
                        }
                    })(i);
                }
            });
        });
    },
    post: function(req, res) {
        req.db.collection("communities.posts.subscribe").insert({post: req.body.post, user: req.user._id, date: new Date()}, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    delete: function(req, res) {
        req.db.collection("communities.posts.subscribe").remove({post: req.params.id, user: req.user._id}, function(err) {
            if (err) return res.status(400).send(err);
            res.status(200).send();
        });
    }
}

function matchPerms(id, type, arr) {
    for (i in arr) {
        if (arr[i].name == id && arr[i].type == type) {
            return arr[i];
        }
    }
    return false;
}

exports.bulkUser = {
    get: function(req, res) {
        req.db.collection("bulkUserImport").find().toArray(function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    delete: function(req, res) {
        req.db.collection("bulkUserImport").remove({}, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    put: function(req, res) {
        req.db.collection("bulkUserImport").update({_id: ObjectID(req.body._id)}, { $set: { username: req.body.username, first_name: req.body.first_name, last_name: req.body.last_name, location: req.body.location, department: req.body.department } }, {upsert: true}, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });

    },
    post: function(req, res) {
        req.db.collection("bulkUserImport").find({location: {$ne: false}, department: {$ne: false}}).toArray(function(err, result) {
            if (err) return res.status(400).send(err);
            if (result.length == 0) return res.status(200).send();
            var users = result;
            for (i in users) {
                users[i].location = users[i].location._id.toString();
                users[i].department = users[i].department._id.toString();
                users[i].role = 'user';
                users[i].disabled = 'false';
                users[i].created = new Date();
                (function(index) {
                    var username = users[index].username.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
                    req.db.collection("user").find({username: {$regex: new RegExp('^' + username + '$', 'i')}}).toArray(function(err, result) {
                        if (err) return res.status(400).send(err);
                        if (result.length == 0) {

                            var password = generatePassword(10)

                            users[index].pin = generatePassword(4);
                            users[index].password = bcrypt.hashSync(password, 10);
                            users[index].info = {created_method: 'ac_bulk'};
                            if (users[index].error) delete users[index].error;
                                email.send(users[index].username, "Welcome to your new " + req.config.name + ' StaffConnect App!', email_builder.register({ user: users[index], config: req.config, pin: users[index].pin, password: password }), req.config )
                                    .then(function(result) {
                                        users[index].tempPass = true;
                                        req.db.collection("user").insert(users[index], function(err, result) {
                                            if (err) return res.status(400).send(err);
                                            req.db.collection("bulkUserImport").remove({_id: users[index]._id}, function(err, result) {
                                                if (err) return res.status(400).send(err);
                                                if (index == users.length-1) {
                                                    res.status(200).send();
                                                }
                                            });
                                        });

                                    }, function(err) {
                                        req.db.collection("bulkUserImport").update({_id: ObjectID(users[index]._id)}, { $set: { error: "Email error: " + err } }, {upsert: true}, function(err, result) {
                                            if (err) return res.status(400).send(err);
                                            if (index == users.length-1) {
                                                res.status(200).send();
                                            }
                                        });

                                    });
                        } else {

                            req.db.collection("bulkUserImport").update({_id: ObjectID(users[index]._id)}, { $set: { error: "Email address is already in use." } }, {upsert: true}, function(err, result) {
                                if (err) return res.status(400).send(err);
                                if (index == users.length-1) {
                                    res.status(200).send();
                                }
                            });
                        }
                    });
                })(i);
            }
        });
    },
    csv: function(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
        });
        form.on('file', function(name, file) {
            var columns = ["username", "first_name", "last_name", "location", "department"];
            csvArray({
                file: file.path,
                columns: columns
            }, function (err, array) {
                if (err) return res.status(400).send(err);

                req.db.collection("permissions").find({}).toArray(function(err, result) {
                    if (err) return res.status(400).send(err);
                    var permissions = result;
                    var result = array;
                    for (i in result) {
                        result[i].location = result[i].location.trim();
                        result[i].department = result[i].department.trim();
                        result[i].first_name = result[i].first_name.trim();
                        result[i].last_name = result[i].last_name.trim();
                        result[i].username = result[i].username.trim();
                        (function(index) {
                            result[index].location = matchPerms(result[index].location, 'location', permissions);
                            result[index].department = matchPerms(result[index].department, 'department', permissions);
                            if (index == result.length-1) {
                                var users = result;
                                req.db.collection("bulkUserImport").insert(users, function(err, result) {
                                    if (err) return res.status(400).send(err);
                                    res.status(200).send(result);
                                });
                            }
                        })(i);
                    }
                });


            });
        });
    }
}

exports.remindUsers = function(req, res) {
  req.db.collection("user").find({pin: {$ne: null}}).toArray(function(err, users) {
    for (i in users) {
      (function(index) {
        var user = users[index];

        var data = {};
        var password = null;
        console.log();
        if (user.tempPass == true) {
            data.password = generatePassword(10)
            password = data.password;
            data.password = bcrypt.hashSync(data.password, 10);
        }
        data.pin = generatePassword(4);

        req.db.collection('user').update({_id: user._id}, {$set: data}, function(err) {
            if (err) throw err;
                email.send(user.username, "Welcome to your new " + req.config.name + ' StaffConnect App!', email_builder.register({ user: user, config: req.config, pin: user.pin, password: password }), req.config )
                    if (index == users.length-1) {
                        res.status(200).send('Emails sent.');
                    }
        });

      })(i);
    }
  });
}
