var uuid = require('node-uuid');
var bcrypt = require('bcrypt');
var generatePassword = require('password-generator');
var Mailgen = require('mailgen');
var ObjectID = require('mongodb').ObjectID;
var nodemailer = require('nodemailer');
var email = require('../lib/email');
var push = require('../lib/push');
var email_builder = require('../lib/email_builder');

function checkPass(password) {
    if (password.length < 8) return 'Password must be 8 characters in length.';
    return true;
}

exports.login = function(req, res) {
    if (!req.body.username) return res.status(400).send('Username is required.');
    if (!req.body.password) return res.status(400).send('Password is required.');
    var username = req.body.username.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&'); 
    req.db.collection('user').find({username: {$regex: new RegExp('^' + username + '$', 'i')}}).toArray(function(err, result) {
        if (err) return res.status(400).send(err);
        if (result.length == 0) {
            return res.status(400).send('Invalid Email Address or Password.');
        }
        if ( bcrypt.compareSync(req.body.password, result[0].password) ) {
            if (result[0].disabled == "true") {
                return res.status(401).send('Account disabled.');
            }
            var token = uuid.v4();
            req.db.collection('session').insert({token: token, userId: result[0]._id}, function(err) {
                if (err) return res.status(400).send(err);
                var data = {};
                if (result[0].location) data.location = result[0].location.toString();
                if (result[0].department) data.department = result[0].department.toString();
                data.lastLogin = new Date();
                req.db.collection('user').update({username: {$regex: new RegExp('^' + req.body.username + '$', 'i')}}, {$set: data});
                res.status(200).send({token: token, user: result[0]});
            });
        } else {
            res.status(400).send('Invalid Username or Password.');
        }
    });
}

exports.logout = function(req, res) {
    req.db.collection('session').remove({token: req.headers.token}, function(err) {
      res.status(200).send();
    });
}
exports.register = function(req, res) {
    if (!req.body.username || req.body.username == "" || !req.body.password || req.body.password == "") {
        return res.status(400).send('Email Address and Password are required.');
    }
    if (checkPass(req.body.password) !== true) {
        return res.status(400).send(checkPass(req.body.password));
    }

    var re = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
    if (!re.test(req.body.username)) {
        return res.status(400).send('Please enter a valid email address.');
    }

    req.db.collection('user').find().toArray(function(err, userCount) {
        if (userCount.length ==0) req.body.role = "admin";
        var username = req.body.username.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&'); 
        req.db.collection('user').find({username: {$regex: new RegExp('^' + username + '$', 'i')}}).toArray(function(err, docs) {
            if (err) throw err;
            if (docs.length > 0) {
                return res.status(400).send('Email account already in use.');
            }
            if (req.body.username.indexOf('@') > -1) {
                req.body.pin = generatePassword(4);
            } else {
                req.body.pin = null;
            }
            req.body.password = bcrypt.hashSync(req.body.password, 10);
            if (req.body.repeatPassword) delete req.body.repeatPassword;
            if (req.body._id) delete req.body._id;
            req.body.created = new Date();
            req.db.collection('user').insert(req.body, function(err) {
                if (err) throw err;
                if (req.body.pin != null) {
                    email.send(req.body.username, "Welcome to your new " + req.config.name + ' StaffConnect App!', email_builder.register({ user: req.body, config: req.config, pin: req.body.pin }), req.config )
                        .then(function(result) {
                            res.status(200).send('Success');
                        }, function(error) {
                            res.status(400).send('Registration Failed.');
                        });
                } else {
                    res.status(200).send('Success');
                }
            });
        });
    });
};
exports.verify = function(req, res) {
    var username = req.params.username.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&'); 
    req.db.collection('user').find({username: {$regex: new RegExp('^' + username + '$', 'i')}}).toArray(function(err, docs) {
        if (err) throw err;
        if (docs.length == 0) {
            docs[0] = {
                first_name: '',
                last_name: ''
            }
        } else if (req.params.pin == docs[0].pin) {
            req.db.collection('user').update({_id: ObjectID(docs[0]._id)},{ $set:{ pin: null } }, function(err, result) {
            });
        }
        if (err) throw err;

        var mailGenerator = new Mailgen({
            theme: 'default',
            product: {
                name: 'The StaffConnect Team',
                link: 'http://staffconnectapp.com/',
                logo: 'http://www.staffconnectapp.com/wp-content/uploads/Logo-Staff-Connect-Inc-Dark-Website.png',
                copyright: 'Copyright © <a href="http://www.staffconnectapp.com/">StaffConnect</a>. All rights reserved.'
            }
        });

        var email = {
            body: {
                intro: 'Account Verified. Please return to the StaffConnect App.',
                name: docs[0].first_name + ' ' + docs[0].last_name,
                greeting: 'Hi',
                signature: 'Thank you'
            }
        };

        var emailBody = mailGenerator.generate(email);
        res.status(200).send(emailBody);
    });
}

exports.forgotPass = function(req, res) {
        if (req.body.username.indexOf('@') == -1) {
            if (!req.body.password) {
                return res.status(400).send('Old password must be supplied with non-email username.');
            }

        }
        var username = req.body.username.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&'); 
    req.db.collection('user').find({username: {$regex: new RegExp('^' + username + '$', 'i')}}).toArray(function(err, docs) {
        if (docs.length == 0) return res.status(400).send('User not found.');
        var User = docs[0];
        var tempPass = false;
        if (req.body.password) {
            if (!req.body.oldPassword) return res.status(400).send('Old password is required.');
            if (!bcrypt.compareSync(req.body.oldPassword, User.password)) {
                return res.status(400).send('Invalid old password.');
            }
            if (checkPass(req.body.password) !== true) {
                return res.status(400).send(checkPass(req.body.password));
            }
            newPass = bcrypt.hashSync(req.body.password, 10);
        } else {
            var Password = generatePassword();
            newPass = bcrypt.hashSync(Password, 10);
            tempPass = true;

            var body = {
                name: User.first_name + ' ' + User.last_name,
                intro: 'Here is your new password: ' + Password,
                outro: ['You will be asked to change this when you log into the app.', 'If you have any problems, please contact <a href="mailto: support@staffconnectapp.com">support@staffconnectapp.com</a> for assistance.'],
                greeting: 'Hi',
                signature: 'Thank you'
            };

            email.send(req.body.username, req.config.name + ' password reset.', body, req.config);
        }

        req.db.collection('user').update({_id: ObjectID(User._id)},{ $set:{ password: newPass, tempPass: tempPass } }, function(err, result) {
            if (err) throw err;
            res.status(200).send('Password reset.');
        });
    });

}



exports.resendVerify = function(req, res) {
    if (!req.body.username) return res.status(400).send('Uername is missing.');
        var username = req.body.username.replace(/[-[\]{}()*+?.,\\^$|#\s]/g, '\\$&');
        req.db.collection('user').find({username: {$regex: new RegExp('^' + username + '$', 'i')}}).toArray(function(err, docs) {
            if (err) throw err;
            if (docs.length == 0) {
                return res.status(400).send('Username doesn\'t exist.');
            }
            if (docs[0].pin == null) return res.status(400).send('User already verified.');
            var data = {};
            var password = null;
            console.log();
            if (docs[0].tempPass == true) {
                data.password = generatePassword(10)
                password = data.password;
                data.password = bcrypt.hashSync(data.password, 10);
            }
            console.log( docs[0], password );
            data.pin = generatePassword(4);

            req.db.collection('user').update({_id: docs[0]._id}, {$set: data}, function(err) {
                if (err) throw err;
                    email.send(docs[0].username, "Welcome to you new " + req.config.name + ' StafConnect App!', email_builder.register({ user: docs[0], config: req.config, pin: data.pin, password: password }), req.config)
                        .then(function(result) {
                            res.status(200).send('Success');
                        }, function(error) {
                            res.status(400).send(error);
                        });
            });
        });
};
