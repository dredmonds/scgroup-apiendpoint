var ObjectID = require('mongodb').ObjectID;
var formidable = require('formidable');
var csvArray = require("csv-to-array");

function matchPerms(id, type, arr) {
    for (i in arr) {
        if (arr[i].name == id && arr[i].type == type) {
            return arr[i];
        }
    }
    return false;
}

exports.bulkUpload = {
    get: function(req, res) {
        req.db.collection("auth2Import").find().toArray(function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    delete: function(req, res) {
        req.db.collection("auth2Import").remove({}, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    },
    put: function(req, res) {
        req.db.collection("auth2Import").update({_id: ObjectID(req.body._id)}, { $set: { data1: req.body.data1, data2: req.body.data2, first_name: req.body.first_name, last_name: req.body.last_name, location: req.body.location, department: req.body.department } }, {upsert: true}, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });

    },
    post: function(req, res) {
        req.db.collection("auth2Import").find({location: {$ne: false}, department: {$ne: false}, data1: {$ne: ''}, data2: {$ne: ''}}).toArray(function(err, result) {
            if (err) return res.status(400).send(err);
            if (result.length == 0) return res.status(200).send();
            var users = result;
            for (i in users) {
                (function(index) {
                  if (users[index].error) delete users[index].error;
                  if (users[index]._id) delete users[index]._id;
                  users[index].used = false;

                  req.db.collection("auth2").insert(users[index], function(err, result) {
                      if (err) return res.status(400).send(err);
                      req.db.collection("auth2Import").remove({_id: users[index]._id}, function(err, result) {
                          if (err) return res.status(400).send(err);
                          if (index == users.length-1) {
                              res.status(200).send();
                          }
                      });
                  });
                })(i);
            }
        });
    },
    csv: function(req, res) {
        var form = new formidable.IncomingForm();
        form.parse(req, function(err, fields, files) {
        });
        form.on('file', function(name, file) {
            var columns = ["data1", "data2", "first_name", "last_name", "location", "department"];
            csvArray({
                file: file.path,
                columns: columns
            }, function (err, array) {
                if (err) return res.status(400).send(err);

                req.db.collection("permissions").find({}).toArray(function(err, result) {
                    if (err) return res.status(400).send(err);
                    var permissions = result;
                    var result = array;
                    for (i in result) {
                        result[i].data1 = result[i].data1.trim();
                        result[i].data2 = result[i].data2.trim();
                        result[i].first_name = result[i].first_name.trim();
                        result[i].last_name = result[i].last_name.trim();
                        result[i].location = result[i].location.trim();
                        result[i].department = result[i].department.trim();
                        (function(index) {
                            result[index].location = matchPerms(result[index].location, 'location', permissions);
                            result[index].department = matchPerms(result[index].department, 'department', permissions);
                            if (index == result.length-1) {
                                var users = result;
                                req.db.collection("auth2Import").insert(users, function(err, result) {
                                    if (err) return res.status(400).send(err);
                                    res.status(200).send(result);
                                });
                            }
                        })(i);
                    }
                });


            });
        });
    }
}

exports.auth2 = {
  get: function(req, res) {
    req.db.collection("auth2").find({}).toArray(function(err, result) {
        if (err) return res.status(400).send(err);
        res.status(200).send(result);
    });
  },
  post: function(req, res) {
    if (!req.body.data1 || !req.body.data2) return res.status(400).send('Registration Failed.');
    req.db.collection("auth2").find({data1: req.body.data1, data2: req.body.data2, used: false}).toArray(function(err, result) {
      if (err) return res.status(400).send(err);
      if (result.length == 0) return res.status(400).send('There is no user account found.');
      res.status(200).send(result[0]);
    });
  },
  put: function(req, res) {
    if (!req.body.data1 || !req.body.data2) return res.status(400).send('Registration Failed.');
    req.db.collection("auth2").update({data1: req.body.data1, data2: req.body.data2}, {$set: {used: true}}, function(err) {
      if (err) return res.status(400).send(err);
      res.status(200).send();
    })
  }
}
