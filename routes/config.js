var ObjectID = require('mongodb').ObjectID;
var formidable = require('formidable');
var lwip = require('lwip');
var uuid = require('node-uuid');
var fs = require('fs');

exports.get = function(req, res) {
    var show = {
        name: 1,
        icon: 1,
        url: 1,
        vpp: 1,
        download: 1,
        "database.name": 1,
        navigationColorScheme: 1,
        features: 1,
        legacy: 1,
        pin: 1
    }
    req.configDb.collection("config").find({'pin': req.params.id}, show).toArray(function(err, result) {
        if (err) return res.status(400).send(err);
        if (result.length == 0) return res.status(400).send('Not found.');
        res.status(200).send(result[0]);
    });
}

exports.clients = {
    get: function(req, res) {
        req.configDb.collection("config").find().toArray(function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result);
        });
    }
}

exports.client = {
    get: function(req, res) {
        req.configDb.collection("config").find({_id: ObjectID(req.params.id)}).toArray(function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result[0]);
        });
    },
    post: function(req, res) {
        req.configDb.collection("config").insert(req.body, function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send(result.insertedIds[0]);
        });
    },
    put: function(req, res) {
        var id = req.body._id;
        delete req.body._id;
        req.configDb.collection("config").find({_id: ObjectID(id)}).toArray(function(err, result) {
            if (result[0].icon !== req.body.icon && fs.existsSync('configfiles/' + result[0].icon)) {
                fs.unlinkSync('configfiles/' + result[0].icon);
            }
            req.configDb.collection("config").update({_id: ObjectID(id)}, { $set: req.body }, function(err, result) {
                if (err) return res.status(400).send(err);
                res.status(200).send(result);
            });
        });
    }
}

exports.upload = function(req, res) {
    var form = new formidable.IncomingForm();
    form.parse(req, function(err, fields, files) {
        for (first in files) break;
        var file = files[first];
        if (file.type.indexOf('image') == -1) return res.status(400).send('File must be an image.');
        var id = uuid.v4();
        var ext = file.name.substr(file.name.lastIndexOf(".") + 1);
        var filename = id + '.' + ext;

        var source = fs.createReadStream(file.path);
        var dest = fs.createWriteStream('configfiles/' + filename);

        source.pipe(dest)
            .on('finish', function () {
                console.log('DONE.');
                res.status(200).send(filename);
            });
    });
}
