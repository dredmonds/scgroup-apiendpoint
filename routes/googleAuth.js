var google = require('googleapis');
var scope1 = 'https://www.googleapis.com/auth/analytics.readonly';
exports.getToken  = function(req, res) {
    var key = require('../google.json');
    var jwtClient = new google.auth.JWT(key.client_email, null, key.private_key, scope1, null);
    jwtClient.authorize(function(err, tokens) {
        res.writeHead(200, {"Content-Type":"text/plain"});res.write(tokens.access_token);res.end();
    });
}
