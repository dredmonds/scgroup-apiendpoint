var push = require('../lib/push');
var ObjectID = require('mongodb').ObjectID;

exports.register = function(req, res) {
    if (!req.body.platform || !req.body.token) return res.status(400).send('Info mising.');

    var platform;
    if (req.body.platform == 'android') platform = 'GCM';
    if (req.body.platform == 'ios') platform = 'APNS';

    if (!req.user.arn) {
      push.registerUser(req.user, req.body.token, req.config, platform)
        .then(function(result) {

          req.db.collection('user').update({_id: req.user._id}, { $set: {arn: result} }, function(err) {
            if (err) return res.status(400).send(err);
            return res.status(200).send();
          });
        }, function(err) {
          return res.status(400).send(err);
        });
    } else {
      push.unregisterUser(req.user, req.config)
        .then(function(result) {
          req.db.collection('user').update({_id: req.user._id}, { $unset: {arn: 1} }, function(err) {
            push.registerUser(req.user, req.body.token, req.config, platform)
              .then(function(result) {
                req.db.collection('user').update({_id: req.user._id}, { $set: {arn: result} }, function(err) {
                  if (err) return res.status(400).send(err);
                  return res.status(200).send();
                });
              }, function(err) {
                return res.status(400).send(err);
              });
          });
        }, function(err) {
          return res.status(400).send(err);
        });
    }
}

exports.global = {
    send: function(req, res) {
        if (!req.body.message) return res.status(400).send('Message required.');
        var data = {
          user: req.user,
          message: req.body.message,
          date: new Date()
        }
        req.db.collection("globalPush").insert(data, function(err) {
          if (err) return res.status(400).send(err);
          req.db.collection("user").find({ arn: {$exists: true} }).toArray(function(err, users) {
          for (i in users) {
              push.sendPush(users[i], req.body.message, 'global', null, req.config);
          }
          res.status(200).send('sent');
          });
        });
    },
    log: function(req, res) {
      req.db.collection("globalPush").find().toArray(function(err, result) {
          console.log(err, result);
        if (err) return res.status(400).send(err);
        return res.status(200).send(result);
      });
    },
    devices: function(req, res) {
      req.db.collection("user").find({ arn: {$exists: true} }).toArray(function(err, devices) {
        if (err) return res.status(400).send(err);
        return res.status(200).send({devices: devices.length});
      });
    }
}

exports.unregister = function(req, res) {
  if (req.user.arn) {
  push.unregisterUser(req.user, req.config)
    .then(function(result) {
      req.db.collection('user').update({_id: req.user._id}, { $unset: {arn: 1} }, function(err) {
        if (err) return res.status(400).send(err);
        return res.status(200).send();
      });
    }, function(err) {
      return res.status(400).send(err);
    });
  } else {
    return res.status(200).send();
  }
}
