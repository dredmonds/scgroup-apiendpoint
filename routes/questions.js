var ObjectID = require('mongodb').ObjectID;

exports.get = function(req, res) {
  req.db.collection('questions').find().toArray(function(err, result) {
      if (err) return res.status(400).send(err);
      res.status(200).send(result);
  });
}
exports.post = function(req, res) {
  if (!req.body.originalQuestion) return res.status(400).send('Missing Question.');
  var data = {
    originalQuestion: req.body.originalQuestion,
    displayQuestion: req.body.originalQuestion,
    answer: null,
    createdAt: new Date(),
    answeredAt: null,
    submittedBy: req.user._id,
    respondedBy: null,
    published: false
  }
  req.db.collection("questions").insert(data, function(err, result) {
    if (err) return res.status(400).send(err);
    res.status(200).send('Question added.');
  });
}
exports.put = function(req, res) {
  if (!req.body.displayQuestion || !req.body.answer || !req.body._id) return res.status(400).send('Information missing.');
  var data = {
    displayQuestion: req.body.displayQuestion,
    answer: req.body.answer,
    answeredAt: new Date(),
    respondedBy: req.user._id,
    published: req.body.published
  }
  req.db.collection("questions").update({_id: ObjectID(req.body._id)}, {$set: data}, function(err, result) {
    if (err) return res.status(400).send(err);
    res.status(200).send('Question published.');
  });
}
exports.delete = function(req, res) {
  if (!req.params._id) return res.status(400).send('ID Missing.');
  req.db.collection("questions").remove({_id: ObjectID(req.params._id)}, function(err) {
    if (err) return res.status(400).send(err);
    res.status(200).send('Question deleted.');
  });
}
