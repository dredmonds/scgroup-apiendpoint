var ObjectID = require('mongodb').ObjectID;
var RSS = require('rss');

exports.get = function(req, res) {
    req.db.collection('other').find({'name': 'rss'}).toArray(function(err, result) {
        if (err) return res.status(400).send(err);
        res.status(200).send(result[0]);
    });
}
exports.post = function(req, res) {
    req.db.collection('other').update({'name': 'rss'}, {'name': 'rss', 'value': req.body.value}, {upsert: true}, function(err) {
        if (err) return res.status(400).send(err);
        res.status(200).send();
    });
}

function match(id, arr) {
    for (i in arr) {
        if (arr[i]._id.toString() == id) {
            return arr[i];
        }
    }
    return id;
}


exports.communities = {
  get: function(req, res) {

    req.db.collection("communities").find({_id: ObjectID(req.params.id), rss: true}).toArray(function(err, communities) {
        if (err) return res.status(400).send(err);
        if (communities.length == 0) return res.status(400).send('Invalid community.');
        var feed = new RSS({
          title: communities[0].name,
          feed_url: 'https://' + req.config.url + '/api/rss/communities/' + req.params.id + '/?pin=' + req.config.pin,
          site_url: 'http://staffconnectapp.com',
          language: 'en',
          pubDate: new Date(),
          ttl: '1'
        });
        req.db.collection("communities.posts").find({community: req.params.id}).sort({date: -1}).toArray(function(err, posts) {
            if (err) return res.status(400).send(err);
            if (posts.length == 0) return res.status(200).send(feed.xml());
            req.db.collection("user").find().toArray(function(err, users) {
                if (err) return res.status(400).send(err);
                req.db.collection("communities.posts.likes").find().toArray(function(err, likes) {
                    if (err) return res.status(400).send(err);
                    req.db.collection("communities.posts.comments").find().toArray(function(err, comments) {
                        if (err) return res.status(400).send(err);
                        req.db.collection("communities.posts.subscribe").find().toArray(function(err, subscribe) {
                            if (err) return res.status(400).send(err);
                            if (posts.length == 0) {
                                res.status(200).send();
                            }
                            for (i in posts) {
                                posts[i].likes = [];
                                posts[i].comments = [];
                                posts[i].subscribe = [];
                                (function(index) {
                                    for (i in comments) {
                                        if (comments[i].post == posts[index]._id) {
                                            posts[index].comments.push(comments[i]);
                                        }
                                    }
                                    for (i in likes) {
                                        if (likes[i].post == posts[index]._id) {
                                            posts[index].likes.push(likes[i].user);
                                        }
                                    }
                                    for (i in subscribe) {
                                        if (subscribe[i].post == posts[index]._id) {
                                            posts[index].subscribe.push(subscribe[i].user);
                                        }
                                    }
                                    for (i in communities) {
                                        if (communities[i]._id.toString() == posts[index].community) {
                                            posts[index].communityInfo = communities[i];
                                        }
                                        
                                    }


                                    posts[index].author = match(posts[index].author, users);
                                    posts[index].actualAuthor = match(posts[index].actualAuthor, users);

                                    feed.item({
                                      title: posts[index].title,
                                      description: posts[index].content,
                                      url: '#',
                                      author: posts[index].author.first_name + ' ' + posts[index].author.last_name,
                                      date: posts[index].date,
                                    });

                                    if (index == posts.length-1) {
                                        res.status(200).send(feed.xml());
                                    }
                                })(i);
                            }
                        });
                    });
                });
            });
        });
    });

  }
}
