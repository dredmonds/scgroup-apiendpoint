var ObjectID = require('mongodb').ObjectID;

exports.get = function(req, res) {
    req.db.collection('other').find({'name': 'ticker'}).toArray(function(err, result) {
        if (err || result.length == 0) return res.status(200).send('Welcome to StaffConnect!');
        res.status(200).send(result[0]);
    });
}
exports.post = function(req, res) {
    console.log(req.body);
    req.db.collection('other').update({'name': 'ticker'}, {'name': 'ticker', 'value': req.body.value}, {upsert: true}, function(err) {
        if (err) return res.status(400).send(err);
        res.status(200).send();
    });
}
