var exec = require('child_process').exec;
var mmmagic = require('mmmagic');
var magic = new mmmagic.Magic(mmmagic.MAGIC_MIME_TYPE);
var mime = require('mime');
var formidable = require('formidable');
var fs = require('fs');
var path = require('path');
var uuid = require('node-uuid');
var q = require('q');
var exec = require('child_process').exec;
var Jimp = require("jimp");
var aws = require('aws-sdk');

var s3 = require('../lib/s3');
var serverConfig = require('../config.json');
var amazonConfig = require('../amazon.json');

var videoUploader = require('../lib/video-uploader');
var transcoderConfig = require('../transcoder.json');

var whitelist = ['image/jpeg', 'image/jpg', 'image/png', 'image/gif', 'video/mp4', 'video/avi', 'video/quicktime', 'video/mpeg', 'video/ogg', 'video/webm', 'application/pdf'];

exports.incoming = function(req, res) {
  var form = new formidable.IncomingForm();
  form.parse(req);
  form.on('file', function(name, file) {
    if (file.size > 1073741824) return res.status(400).send('Files must be under 1GB in size.');
    magic.detectFile(file.path, function(err, result) {
      var id = uuid.v4();
      file.id = id;
      file.type = result;
      file.ext = mime.extension(result);
      file.filename = id + '.' + file.ext;
      file.thumbname = id + '-thumb.' + file.ext;
      file.previewname = id + '-preview.' + file.ext;
      var source = fs.createReadStream(file.path)
      var dest = fs.createWriteStream(path.resolve(__dirname, '../uploads/' + file.filename))
      source.pipe(dest)
      .on('finish', function () {
        if (whitelist.indexOf(file.type) == -1) return res.status(400).send('File type not supported (' + file.type + ').');
        if (file.type.indexOf('image') > -1)  {
          handleImage(req, res, file);
        } else if (file.type.indexOf('video') > -1) {
          handleVideo(req, res, file);
        } else {
          handleOther(req, res, file);
        }

      });
    });
  });
}

function handleImage(req, res, file) {
  fixImage(path.resolve(__dirname, '../uploads/' + file.filename), path.resolve(__dirname, '../uploads/' + file.filename))
  .then(function() {
    resizeImage(path.resolve(__dirname, '../uploads/' + file.filename), path.resolve(__dirname, '../uploads/' + file.thumbname), 300, 300)
    .then(function() {
      resizeImage(path.resolve(__dirname, '../uploads/' + file.filename), path.resolve(__dirname, '../uploads/' + file.previewname), 1280, 720)
      .then(function() {

        s3.upload(req.config.database.name, 'images/'+file.filename, fs.createReadStream( path.resolve(__dirname, '../uploads/' + file.filename) ))
        .then(function(result) {
          fs.unlinkSync(path.resolve(__dirname, '../uploads/' + file.filename));
          file.filename = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/images/' + file.filename;
          s3.upload(req.config.database.name, 'images/'+file.thumbname, fs.createReadStream( path.resolve(__dirname, '../uploads/' + file.thumbname) ))
          .then(function(result) {
            fs.unlinkSync(path.resolve(__dirname, '../uploads/' + file.thumbname));
            file.thumbname = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/images/' + file.thumbname;
            s3.upload(req.config.database.name, 'images/'+file.previewname, fs.createReadStream( path.resolve(__dirname, '../uploads/' + file.previewname) ))
            .then(function(result) {
              fs.unlinkSync(path.resolve(__dirname, '../uploads/' + file.previewname));
              file.previewname = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/images/' + file.previewname;
              var filenames = [{filename: file.filename, thumbname: file.thumbname, previewname: file.previewname, type: 'image', extension: '.' + file.ext}];
              res.status(200).send(filenames);
            }, function(err) {
              return res.status(400).send(err);
            });
          }, function(err) {
            return res.status(400).send(err);
          });
        }, function(err) {
          return res.status(400).send(err);
        });
      }, function(err) {
        res.status(400).send(err);
      });
    }, function(err) {
      res.status(400).send(err);
    });
  }, function(err) {
    res.status(400).send(err);
  });
}

function handleVideo(req, res, file) {
    var outputFileName = file.id;

    var options = {
        inputFilePath: path.resolve(__dirname, '../uploads/' + file.filename),
        rawVideoKey: req.config.database.name + '/videos/' + outputFileName + '/raw-' + file.filename,
        outputVideoDir: req.config.database.name + '/videos/',
        outputVideoName: outputFileName + '/' + outputFileName,
        s3BucketIn: serverConfig.bucket,
        pipelineId: serverConfig.pipelineId,
        presets: transcoderConfig.presets,
        userMetadata: {
            code: req.headers.code,
            attachmentUUID: outputFileName
        }
    };
    if (req.user) {
        options.userMetadata.authorId = req.user._id.toString();
    }
    videoUploader.uploadVideoToS3(options)
    .then(function (data) {
        if (!data.Job.Outputs[0] || !data.Job.Outputs[0].ThumbnailPattern) {
            throw new Error('No thumbnail was created');
        }

        var previewname = data.Job.Outputs[0].ThumbnailPattern.replace(/\{count\}/, '00001');
        var thumbname = data.Job.Outputs[1].ThumbnailPattern.replace(/\{count\}/, '00001');

        var addURLPrefix = function (filename) {
            return amazonConfig.s3RegionUrl + serverConfig.bucket + '/' + data.Job.OutputKeyPrefix + filename;
        };

        var filenameList = data.Job.Playlists.map(function (output) {
            return {
                uuid: outputFileName,
                filename: addURLPrefix(output.Name) + transcoderConfig.outputFileExtension,
                previewname: addURLPrefix(previewname) + transcoderConfig.outputThumbnailFileExtension,
                thumbname: addURLPrefix(thumbname) + transcoderConfig.outputThumbnailFileExtension,
                type: 'video',
                extension: transcoderConfig.outputFileExtension
            };
        });
        res.status(200).send(filenameList);
    })
    .catch(function (err) {
        console.error(err.stack);
        res.status(500).send(err.message);
    });
}

function handleOther(req, res, file) {
  s3.upload(req.config.database.name, 'other/'+file.filename, fs.createReadStream( path.resolve(__dirname, '../uploads/' + file.filename) ))
  .then(function(result) {
    fs.unlinkSync(path.resolve(__dirname, '../uploads/' + file.filename));
    file.filename = 'https://s3-eu-west-1.amazonaws.com/' + serverConfig.bucket +  '/' + req.config.database.name + '/other/' + file.filename;
    var filenames = [{filename: file.filename, thumbname: undefined, type: 'other', extension: '.' + file.ext}];
    res.status(200).send(filenames);
  }, function(err) {
    return res.status(400).send(err);
  });
}

function resizeImage(source, destination, w, h) {
  var deferred =  q.defer();
  Jimp.read(source, function (err, image) {
    if (err) return deferred.reject(err);
    image
    .cover(w, h)
    .quality(100)
    .write(destination, function() {
      deferred.resolve();
    });
  });
  return deferred.promise;
}
function fixImage(source, destination) {
  var deferred =  q.defer();
  Jimp.read(source, function (err, image) {
    if (err) return deferred.reject(err);
    image
    .scale(1)
    .quality(100)
    .write(destination, function() {
      deferred.resolve();
    });
  });
  return deferred.promise;
}

function convertVideo(source, destination) {
  //Leaving options open for converting video.
  var deferred =  q.defer();
  deferred.resolve();
  return deferred.promise;
}
