require('newrelic');
var express = require('express');
var ObjectID = require('mongodb').ObjectID;
var fs = require("fs");
var bodyParser = require('body-parser');
var morgan = require('morgan');
var path = require('path');
var https = require('https');
var http = require('http');
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var cors = require('cors');
var formidable = require('formidable');
var Log = require('./lib/log');
var request = require('request');

var serverConfig = require('./config.json');

var Middleware = require('./lib/middleware');
var Config = require('./routes/config');
var Auth = require('./routes/auth');
var Auth2 = require('./routes/auth2');
var Push = require('./routes/push');
var Admin = require('./routes/admin');
var RSS = require('./routes/rss');
var Ticker = require('./routes/ticker');
var googleAuth = require('./routes/googleAuth');
var Upload = require('./routes/upload');
var Questions = require('./routes/questions');
var amazonConfig = require('./amazon.json');
var transcoderConfig = require('./transcoder.json');
var mongo;

var serverConfig = require('./config.json');

//Legacy
var library = require('./legacy_routes/library');
var events = require('./legacy_routes/events');

var aws = require('aws-sdk');

// To test SNS locally, you will need to expose your server to the internet. See https://ngrok.com/.
// See the snsProtocol option in amazon.json and url option in config.json.
var sns = new aws.SNS(amazonConfig);

MongoClient.connect(serverConfig.database, { reconnectTries: 15, reconnectInterval: 1000, readPreference: mongodb.Server.READ_SECONDARY }, function(err, db) {
    if (err) throw err;
    mongo = db;

    var app = express();
    var route_args = {
        app: app,
        express: express,
    };

    // We need this to allow bodyParser to work with Amazon SNS
    var snsOverrideContentType = function() {
        return function(req, res, next) {
            if (req.headers['x-amz-sns-message-type']) {
                req.headers['content-type'] = 'application/json;charset=UTF-8';
            }
            next();
        };
    }

    var snsAcceptSubscription = function() {
        return function(req, res, next) {
            var snsMessageType = req.headers['x-amz-sns-message-type'];

            if (snsMessageType == 'SubscriptionConfirmation') {
                // Confirm the subscription with Amazon SNS.
                request(req.body.SubscribeURL);
                return res.status(200).send();
            }
            next();
        };
    }

    app
        .use(snsOverrideContentType())
        .use(bodyParser.json())
        .use(morgan('dev'))
        .use(snsAcceptSubscription())

        .use(cors())

        .options('/*', function(res,res) {
          return res.status(200).send();
        })

        .use(function(req, res, next) {
          req.configDb = mongo.db('switchboard');
          next();
        })

        .get('/health-check.html', function(req, res) {
          req.configDb.collection('config').find().toArray(function(err, result) {
            if (err) return res.status(400).send(err);
            res.status(200).send('OK');
          });
        })

        .get('/load-test.html', function(req, res) {
          var testDB = mongo.db('testdatabase');
          testDB.collection('data').insert({name: 'John Doe', age: 25, location: 'United States of America.'}, function(err, result) {
            if (err) return res.status(400).send(err);
            testDB.collection('data').find({name: 'John Doe'}).toArray(function(err, result) {
              if (err) return res.status(400).send(err);
              testDB.collection('data').remove({name: 'John Doe'}, function(err) {
                if (err) return res.status(400).send(err);
                res.status(200).send('OK');
              });
            });
          });
        })

        .use("/uploads", express.static(path.join(__dirname, 'uploads')))
        .use("/configfiles", express.static(path.join(__dirname, 'configfiles')))
        .use("/assets", express.static(path.join(__dirname, 'assets')))

        .get('/config/:id', Config.get)
        .use(function(req, res, next) {
            if (req.query.pin) {
                query = {"pin": req.query.pin};
            } else if (req.headers.code) {
                query = { "pin": req.headers.code };
            } else {
                // For Amazon SNS notifications related to video transcoder.
                var jobData = null
                if (req.body.Message) {
                    try {
                        jobData = JSON.parse(req.body.Message);
                    } catch (err) {
                        console.error(err);
                    }
                }
                if (jobData) {
                    var metadata = jobData.userMetadata;
                    query = { pin: metadata.code };
                } else {
                    query = { url: req.headers.host };
                }
            }
            req.configDb.collection('config').find(query).toArray(function(err, result) {
                if (err) throw err;
                var log = {
                    date: new Date(),
                    body: req.body,
                    headers: req.headers,
                    url: req.originalUrl,
                    ip: req.ip,
                    ua: req.headers['user-agent']
                }
                log = JSON.parse( JSON.stringify( log ) );
                if (log.body.password) delete log.body.password;
                if (log.body.repeatPassword) delete log.body.repeatPassword;
                if (log.body.oldPassword) delete log.body.oldPassword;

                req.log = JSON.parse( JSON.stringify( log ) );
                var Config = result[0];
                if (Config == undefined) return res.status(404).send('Server not found.');
                if (Config.disabled == 'true') return res.status(401).send("Instance Disabled. Please contact your Instance Administrator.");
                req.db = mongo.db(Config.database.name);
                req.config = Config;
                req.config.url = serverConfig.url;
                res.on('finish', function() {
                    Log.log(req.config.database.name, req.log);
                });

                next();
            });
        })

        // Transcoder SNS HTTP endpoints
        .post(transcoderConfig.snsTopics.completed.endpoint, function(req, res) {
            var snsMessageType = req.headers['x-amz-sns-message-type'];
            if (snsMessageType == 'Notification') {
                // We're not verifying the signature of the SNS message at the moment.
                // See http://docs.aws.amazon.com/sns/latest/dg/SendMessageToHttp.verify.signature.html
                // At the moment this is OK because we're just using the notification to set a
                // 'videosReady' flag on a post in MongoDB but we may want to do this in the future if the need arises.
                var jobData;
                try {
                    jobData = JSON.parse(req.body.Message);
                } catch (err) {
                    console.error(err);
                }
                if (jobData) {
                    var metadata = jobData.userMetadata;

                    var postsCollection = req.db.collection('communities.posts');

                    var authorId = metadata.authorId;
                    var attachmentUUID = metadata.attachmentUUID;

                    // TODO: If we index the author field on the 'communities.posts' collection, we
                    // can make this 'find' operation faster.
                    console.log(authorId, attachmentUUID);
                    postsCollection.find({author: authorId, 'attachments.uuid': attachmentUUID}).toArray()
                    .then(function (items) {
                        var post = items[0];
                        if (!post) {
                            var noPostFoundError = new Error('Could not find a post for attachment ' + attachmentUUID);
                            noPostFoundError.name = 'NoPostFoundForAttachmentError';
                            throw noPostFoundError;
                        }

                        var attachmentIndex;
                        post.attachments.forEach(function (attachment, index) {
                            if (attachment.uuid == attachmentUUID) {
                                attachmentIndex = index;
                                attachment.processed = new Date();
                            }
                        });

                        var updateSelector = {author: authorId, 'attachments.uuid': attachmentUUID};
                        var updateAction = {$set: {}};
                        updateAction.$set['attachments.' + attachmentIndex] = post.attachments[attachmentIndex];

                        return postsCollection.update(updateSelector, updateAction);
                    })
                    .then(function () {
                        // Fetch a frash copy of the same post again to account for the odd case were
                        // the 'processed' status of another attachment may have changed at the same time.
                        return postsCollection.find({author: authorId, 'attachments.uuid': attachmentUUID}).toArray()
                    })
                    .then(function (items) {
                        // Check if all attachments have been processed, if so, flag the post
                        // with videosReady = true.
                        var post = items[0];
                        var allVideoAttachmentsAreProcessed = true;
                        post.attachments.forEach(function (attachment) {
                            if (attachment.type == 'video' && attachment.processed == null) {
                                allVideoAttachmentsAreProcessed = false;
                            }
                        });

                        if (allVideoAttachmentsAreProcessed) {
                            var updateSelector = {author: authorId, 'attachments.uuid': attachmentUUID};
                            var updateAction = {$set: {videosReady: true}};

                            return postsCollection.update(updateSelector, updateAction);
                        }
                    })
                    .catch(function (err) {
                        console.error(err);
                        if (err.name == 'NoPostFoundForAttachmentError') {
                            // Maybe post was deleted. In this case SNS should not retry.
                            res.status(200).send();
                        } else {
                            res.status(500).send();
                        }
                    });
                } else {
                    res.status(200).send();
                }
            } else {
                res.status(200).send();
            }
        })
        .post(transcoderConfig.snsTopics.warnings.endpoint, function(req, res) {
            var snsMessageType = req.headers['x-amz-sns-message-type'];

            if (snsMessageType == 'Notification') {
                // TODO: Better warning logging; add video details.
                console.warn('Transcode video warning');
                res.status(200).send();
            }
        })
        .post(transcoderConfig.snsTopics.errors.endpoint, function(req, res) {
            var snsMessageType = req.headers['x-amz-sns-message-type'];

            if (snsMessageType == 'Notification') {
                // TODO: Better error logging; add video details.
                console.error('Failed to transcode video');
                res.status(200).send();
            }
        })

        .get('/version', function(req,res) {
            res.status(200).send({version: {android: '0.1.0', ios: '0.1.0'}, forced: false});
        })

        .get('/rss/communities/:id', RSS.communities.get)

        .post('/auth/login', Auth.login)
        .post('/auth/register', Auth.register)
        .get('/verify/:username/:pin', Auth.verify)
        .post('/auth/forgotpass', Auth.forgotPass)
        .post('/auth/resendverify', Auth.resendVerify)
        .post('/auth/logout', Auth.logout)

        .put('/auth2', Auth2.auth2.put)
        .post('/auth2', Auth2.auth2.post)

        require("./legacy_routes/terms.js")(route_args);
      app
        .use(Middleware.restrict)

        .get('/auth2/import', Middleware.admin, Auth2.bulkUpload.get)
        .delete('/auth2/import', Middleware.admin, Auth2.bulkUpload.delete)
        .put('/auth2/import', Middleware.admin, Auth2.bulkUpload.put)
        .post('/auth2/import', Middleware.admin, Auth2.bulkUpload.post)
        .post('/auth2/import/csv', Middleware.admin, Auth2.bulkUpload.csv)

        .get('/auth2', Middleware.admin, Auth2.auth2.get)

        .get('/googleAuth',   googleAuth.getToken)
        .post('/push/register', Push.register)
        .post('/push/unregister', Push.unregister)

        .get('/admin/users', Middleware.admin, Admin.users.get)
        .put('/admin/users', Middleware.admin, Admin.users.put)
        .post('/admin/users', Middleware.admin, Admin.users.post)

        .post('/push/send', Middleware.admin, Push.global.send)
        .get('/push/log', Middleware.admin, Push.global.log)
        .get('/push/devices', Middleware.admin, Push.global.devices)

        .get('/bulkUserUpload', Middleware.admin, Admin.bulkUser.get)
        .post('/bulkUserUpload', Middleware.admin, Admin.bulkUser.post)
        .put('/bulkUserUpload', Middleware.admin, Admin.bulkUser.put)
        .delete('/bulkUserUpload', Middleware.admin, Admin.bulkUser.delete)
        .post('/bulkUserUpload/csv', Middleware.admin, Admin.bulkUser.csv)

        .get('/auth/locations', Admin.locations.get)
        .post('/auth/locations', Middleware.admin, Admin.locations.post)
        .put('/auth/locations', Middleware.admin, Admin.locations.put)
        .delete('/auth/locations/:id', Middleware.admin, Admin.locations.delete)

        .get('/auth/departments', Admin.departments.get)
        .post('/auth/departments', Middleware.admin, Admin.departments.post)
        .put('/auth/departments', Middleware.admin, Admin.departments.put)
        .delete('/auth/departments/:id', Middleware.admin, Admin.departments.delete)

        .get('/communities/:type', Admin.communities.get)
        .post('/communities/:type', Middleware.admin, Admin.communities.post)
        .put('/communities', Middleware.admin, Admin.communities.put)
        .delete('/communities/:id', Middleware.admin, Admin.communities.delete)

        .get('/questions', Questions.get)
        .post('/questions', Questions.post)
        .put('/questions', Middleware.admin, Questions.put)
        .delete('/questions/:_id', Middleware.admin, Questions.delete)

        .get('/posts/:id', Admin.posts.get)
        .post('/posts', Admin.posts.post)
        .put('/posts', Admin.posts.put)
        .delete('/posts/:id', Admin.posts.delete)

        .get('/likes/:id', Admin.likes.get)
        .post('/likes', Admin.likes.post)
        .delete('/likes/:id', Admin.likes.delete)

        .get('/comments/:id', Admin.comments.get)
        .post('/comments', Admin.comments.post)
        .put('/comments', Admin.comments.put)
        .delete('/comments/:id', Admin.comments.delete)

        .get('/subscribe/:id', Admin.subscribe.get)
        .post('/subscribe', Admin.subscribe.post)
        .delete('/subscribe/:id', Admin.subscribe.delete)

        .get('/ticker', Ticker.get)
        .post('/ticker', Middleware.admin, Ticker.post)

        .get('/rss', RSS.get)
        .post('/rss', Middleware.admin, RSS.post)

        .get('/admin/clients', Middleware.admin, Middleware.sc, Config.clients.get)

        .get('/admin/client/:id', Middleware.admin, Middleware.sc, Config.client.get)
        .put('/admin/client', Middleware.admin, Middleware.sc, Config.client.put)
        .post('/admin/client', Middleware.admin, Middleware.sc, Config.client.post)
        .post('/admin/client/upload', Middleware.admin, Middleware.sc, Config.upload)

        .get('/directory', Admin.userDirectory.get)

        .put('/user', Admin.user.put)
        .post('/avatar', Admin.user.avatar)

        .post('/admin/remindusers', Middleware.admin, Admin.remindUsers)

        .post('/upload', Upload.incoming)

        //Legacy
        .get('/library', library.library)
        .post('/library-new-item', library.libraryNewItem)
        .post('/library-update-item', library.libraryUpdateItem)
        .post('/library-delete-item', library.libraryDeleteItem)

        .get('/library-categories', library.libraryCategories)
        .post('/library-categories-new-item', library.libraryCategoriesNewItem)
        .post('/library-categories-update-item', library.libraryCategoriesUpdateItem)
        .post('/library-categories-delete-item', library.libraryCategoriesDeleteItem)

        .get('/events', events.events)
        .post('/events-new-item', events.eventsNewItem)
        .post('/events-update-item', events.eventsUpdateItem)
        .post('/events-delete-item', events.eventsDeleteItem)


        require("./legacy_routes/newquiz.js")(route_args);
        require("./legacy_routes/support.js")(route_args);
        require("./legacy_routes/terms.js")(route_args);
        require("./legacy_routes/auth.js")(route_args);

    // Wait for all SNS subscriptions to be active before launching the HTTP server.
    http.createServer(app).listen(8080, '0.0.0.0');

    // Subscribe to all SNS topics related to video transcoding.
    Object.keys(transcoderConfig.snsTopics).forEach(function(action) {
        var details = transcoderConfig.snsTopics[action];
        subscribeToTranscoderSNSTopic(details.topicArn, details.endpoint);
    });

    console.log('Running V2.');
});

function subscribeToTranscoderSNSTopic(topicArn, endpointPath) {
    var endpoint = amazonConfig.snsProtocol + '://' +  serverConfig.url + '/api' + endpointPath;

    sns.subscribe({
        Protocol: amazonConfig.snsProtocol, // http or https
        TopicArn: topicArn,
        Endpoint: endpoint
    }, function(err, data) {
        if (err) {
            throw err;
        } else {
            console.log('Subscribed to SNS topic ' + topicArn + ' on endpoint: ' + endpoint);
        }
    });
}
