var assert = require('assert');
var util = require('util');
var videoUploader = require('../lib/video-uploader');

var options = {
    inputFilePath: __dirname + '/assets/test.webm',
    rawVideoKey: 'test-video.mp4',
    outputVideoDir: 'transcoded-videos/',
    outputVideoName: 'test-video/test-video',
    keepLocalUploadedFile: true,
    userMetadata: {
        postId: '123'
    }
};

describe('video-uploader tests', function () {
    it('should upload a .webm video without errors', function (done) {
        // Just check that it doesn't crash. Check manually in AWS S3 to check
        // that the file was created correctly.
        videoUploader.uploadVideoToS3(options)
        .then(function (data) {
            console.log('Result:', util.inspect(data, {depth: 4}));
            done();
        })
        .catch(function (err) {
            assert.ifError(err);
            done();
        });
    });
});
